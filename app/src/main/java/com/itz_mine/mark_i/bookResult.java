package com.itz_mine.mark_i;

/**
 * Created by root on 1/6/16.
 */
public class bookResult {
    String title;
    String name;
    String image_url;
    String book_id;

    String rent_price;

    public String getResale_price() {
        return resale_price;
    }

    public void setResale_price(String resale_price) {
        this.resale_price = resale_price;
    }

    public String getRent_price() {
        return rent_price;
    }

    public void setRent_price(String rent_price) {
        this.rent_price = rent_price;
    }

    String resale_price;



    public String getIsbn13() {
        return isbn13;
    }

    public void setIsbn13(String isbn13) {
        this.isbn13 = isbn13;
    }

    String isbn13;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getBook_id() {
        return book_id;
    }

    public void setBook_id(String book_id) {
        this.book_id = book_id;
    }
}