package com.itz_mine.mark_i;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseUser;

/**
 * Created by root on 8/7/15.
 */
public class ParseApplication extends Application{
    @Override
    public void onCreate() {
        super.onCreate();

        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();

        ParseACL.setDefaultACL(defaultACL, true);
    }
}
