package com.itz_mine.mark_i;


import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class wallet_fragment extends Fragment {

    TextView walletBalance;
    TextView transferToBank;
    TextView miniStatement;
    user_creds user;
    EditText enter_amount_edittext;


    public wallet_fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView =  inflater.inflate(R.layout.fragment_wallet_fragment, container, false);
        walletBalance = (TextView) fragmentView.findViewById(R.id.wallet_balance_textview);
        TextView add_money_status_display_field = (TextView) fragmentView.findViewById(R.id.add_money_status_display);
        transferToBank = (TextView) fragmentView.findViewById(R.id.transfter_to_bank_textview);
        miniStatement = (TextView) fragmentView.findViewById(R.id.mini_statement_textview);
        enter_amount_edittext = (EditText) fragmentView.findViewById(R.id.add_to_wallet_amount);

        Bundle userdata = this.getArguments();
        if (userdata == null) {
            Log.e("#### "+ Utils.getLogMsgId(), "EMPTY BUNDLE RECEIVED");
        } else
            Log.d("#### "+ Utils.getLogMsgId(), "BUNDLE IS NOT EMPTY");

        user = (user_creds) userdata.getSerializable("user");
        if (user == null) {
            Log.e ("#### "+ Utils.getLogMsgId(), "GOT NULL user_creds FROM BUNDLE");
        } else
            Log.d ("#### "+ Utils.getLogMsgId(), "Got data from bundle: "+user.getFull_name());

        String rupee_symbol = getResources().getString(R.string.Rs);
        walletBalance.setText("Wallet Balance : "+rupee_symbol+" "+user.getWallet()+" /-");
        add_money_status_display_field.setVisibility(View.INVISIBLE);

        enter_amount_edittext.setText("");

        return fragmentView;
    }

    public void updateWallet (int wallet) {
        String rupee_symbol = getResources().getString(R.string.Rs);
        walletBalance.setText("Wallet :  "+rupee_symbol+" "+wallet+" /-");
    }

}
