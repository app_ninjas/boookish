package com.itz_mine.mark_i;

public class Constants {

	public static String BASE_URL = "https://www.cognalys.com/api/v2/request_missed_call/";
	public static String BASE_URL2 ="https://www.cognalys.com/api/v2/confirm_verification/";
	public static String ONE = "MISSING CREDENTIALS";
	public static String TWO = "MISSING REQUIRED VALUES";
	public static String THREE = "MISSING PROPER NUMBER";
	public static String FOUR = "VERIFICATION SUCCESS";
	public static String FIVE="NUMBER IS NOT CORRECT";
	public static String SIX="MOBLIE NUMBER VERIFICATION CANCELED";
	public static String SEVEN="NETWORK ERROR CANNOT BE VERIFIED";
	public static String EIGHT="MOBLIE NUMBER VERIFICATION FAILED, NO INTERNET";
	public static final int SUCCESS_RESULT = 0;
	public static final int FAILURE_RESULT = 1;
	public static final String PACKAGE_NAME = "com.google.android.gms.location.sample.locationaddress";
	public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
	public static final String RESULT_DATA_KEY = PACKAGE_NAME +	".RESULT_DATA_KEY";
	public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME +	".LOCATION_DATA_EXTRA";



}
// https://www.cognalys.com/api/v1/otp/?app_id=YOUR_OTP_APP_ID&access_token=YOUR_OTP_ACCESS_TOKEN&mobile=MOBILE