package com.itz_mine.mark_i;

import android.content.Intent;
import android.content.pm.PackageInstaller;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.tv.TvInputService;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;


public class Login extends ActionBarActivity {
    private static TextView display_msg = null;
    private static boolean is_First;
    private EditText email_editText = null;
    private EditText password_editText = null;
    Button loginButton;
    Button forgot_password_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setBookeshLogo();
        loginButton = (Button) findViewById(R.id.log_in_button);
        forgot_password_button = (Button) findViewById(R.id.forgot_password_button);
        setEmailnPasswordEditTexts();
        is_First = false;
    }

    public void onForgotPasswordClick (View v) {
        EditText email_id_edit_text = (EditText) findViewById(R.id.email_id_edittext);
        String email_id = email_id_edit_text.getText().toString();

        ParseUser.requestPasswordResetInBackground(email_id,
                new RequestPasswordResetCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            displaymessage("Check your email for resetting the password.");
                        } else {
                            displaymessage(e.getMessage());
                        }
                    }
                });
    }

    public void onSignUpClick (View v) {
        Intent signUpIntent = new Intent(this,SignUpActivity.class);
        startActivity(signUpIntent);
    }

    public void onLogInClick (View v) {
        email_editText = (EditText) findViewById(R.id.email_id_edittext);
        password_editText = (EditText) findViewById(R.id.password_edittext);

        String email_id = email_editText.getText().toString();
        String password = password_editText.getText().toString();

        Button loginButton = (Button) findViewById(R.id.log_in_button);
        loginButton.setBackgroundColor(getResources().getColor(R.color.clicked_Button_color));

        logInUser(email_id, password, loginButton);
    }

    public void startWelcomeActivity () {
        Intent welcomeIntent = new Intent(Login.this, WelcomeActivity.class);
        startActivity(welcomeIntent);
        finish();
    }

    public void logInUser (final String email_id, String password, final Button loginButton) {
        ParseUser.logInInBackground(email_id, password,
                new LogInCallback() {
                    public void done(ParseUser user, ParseException e) {
                        if (user != null) {
                            if (is_First == true)
                                return;
                            // If user exist and authenticated, send user to Welcome.class
                            is_First = true;
                            Log.d("#### "+Utils.getLogMsgId(), "Setting button as disabled");
                            startWelcomeActivity();
                        } else {
                            displaymessage(e.getMessage());
                            loginButton.setBackgroundColor(getResources().getColor(R.color.transparent_Button_color));
                        }
                    }
                });
    }

    public void setBookeshLogo () {
        TextView bookesh_logo = (TextView) findViewById(R.id.bookesh_logo);
        Typeface blackJack_tf = Typeface.createFromAsset(getAssets(), "fonts/black_jack.ttf");
        bookesh_logo.setTypeface(blackJack_tf);
    }

    public void setEmailnPasswordEditTexts () {
        email_editText = (EditText) findViewById(R.id.email_id_edittext);
        password_editText = (EditText) findViewById(R.id.password_edittext);

        email_editText.addTextChangedListener(watcher);
        password_editText.addTextChangedListener(watcher);
        loginButton.setEnabled(false);
        loginButton.setTextColor(Color.GRAY);
        forgot_password_button.setEnabled(false);
        forgot_password_button.setTextColor(Color.GRAY);
    }

    private final TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if ((email_editText.getText().length() == 0) || (password_editText.getText().length() == 0)) {
                loginButton.setTextColor(Color.GRAY);
                loginButton.setEnabled(false);
            }else {
                loginButton.setTextColor(getResources().getColor(R.color.bookesh_logo_color));
                loginButton.setEnabled(true);
            }

            if (email_editText.getText().length() == 0) {
                forgot_password_button.setEnabled(false);
                forgot_password_button.setTextColor(Color.GRAY);
            } else {
                forgot_password_button.setEnabled(true);
                forgot_password_button.setTextColor(getResources().getColor(R.color.bookesh_logo_color));
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public void displaymessage (String msg) {
        Resources res = getResources();
        LinearLayout linear_layout_buttons = (LinearLayout) findViewById(R.id.Linear_Layout_Buttons);
        if (display_msg == null) {
            display_msg = new TextView(this);
            linear_layout_buttons.addView(display_msg);
        }

        display_msg.setText(msg);
        display_msg.setTextSize(15);
        display_msg.setTextColor(res.getColor(R.color.editText_highlight_color));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
    }

    public void create_user_cred (final ParseUser user, final String user_id, final String full_name) {
        /* Fetch user info from user_creds db */
        final boolean[] is_match = {false};
        ParseQuery query = new ParseQuery("user_creds");
        Log.d("#### "+Utils.getLogMsgId(), user_id);
        query.whereEqualTo("user_id", user_id);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                Log.d("#### "+Utils.getLogMsgId(), "Done");
                if (e == null) {
                    Log.d("#### "+Utils.getLogMsgId(), "TEST");
                    for (ParseObject object : list) {
                        is_match[0] = true;
                    }

                    if (is_match[0] == false) {
                        /* Saving user data in the pseudo user class user_creds so as to manage one account for multiple logins */
                        ParseObject user_creds = new ParseObject("user_creds");
                        user_creds.put("user_id", user_id);
                        user_creds.put("full_name", full_name);
                        user_creds.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    user.saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            if (e == null) {
                                                startWelcomeActivity();
                                            } else {
                                                Log.d("#### "+Utils.getLogMsgId(), e.getMessage());
                                            }
                                        }
                                    });
                                } else {
                                    Log.d ("#### "+Utils.getLogMsgId(), e.getMessage());
                                }
                            }
                        });
                    } else {
                        user.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    startWelcomeActivity();
                                } else {
                                    Log.d("#### "+Utils.getLogMsgId(), e.getMessage());
                                }
                            }
                        });
                    }
                } else {
                    Log.d("#### "+Utils.getLogMsgId(), e.getMessage());
                }
            }
        });
    }

    public void onFBLoginClick (View v) {
        Collection<String> permissions = Arrays.asList("public_profile", "user_friends", "email");
        Log.d("#### " + Utils.getLogMsgId(), "FBLOGIN: ENTERED FUNC");

        final Button fb_signin_btn = (Button) findViewById(R.id.fb_sign_in_button);
        fb_signin_btn.setBackgroundColor(getResources().getColor(R.color.clicked_Button_color));
        fb_signin_btn.setEnabled(false);

        ParseFacebookUtils.logInWithReadPermissionsInBackground(this, permissions, new LogInCallback() {
            @Override
            public void done(final ParseUser user, ParseException err) {
                fb_signin_btn.setBackgroundColor(getResources().getColor(R.color.transparent_Button_color));
                fb_signin_btn.setEnabled(true);
                if (user == null) {
                    displaymessage("facebook login cancelled by user");
                } else if (user.isNew()) {
                    displaymessage("Signed Up Successfully");
                    Log.d("#### "+Utils.getLogMsgId(), "Signed up successfully");
                    GraphRequest request = GraphRequest.newMeRequest(
                            AccessToken.getCurrentAccessToken(),
                            new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(JSONObject object, GraphResponse response) {
                                    Log.v("LoginActivity "+Utils.getLogMsgId(), response.toString());
                                    try {
                                        final String email = object.getString("email");
                                        final String name = object.getString("name");
                                        Log.d("#### "+Utils.getLogMsgId(), "email: "+email);
                                        Log.d("#### "+Utils.getLogMsgId(), "name: "+name);
                                        user.put("user_id", email);

                                        create_user_cred(user,email, name);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        displaymessage("Something went wrong. Please try again.");
                                        Log.d("#### "+Utils.getLogMsgId(), "Failure in fetching GraphRequest");
                                        return;
                                    }
                                }
                            });

                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id,name,email,gender,birthday,location");   //and location parameter
                    request.setParameters(parameters);
                    request.executeAsync();
                } else {
                    displaymessage("Logged In Successfully");
                    startWelcomeActivity();
                }
            }
        });
    }
}
