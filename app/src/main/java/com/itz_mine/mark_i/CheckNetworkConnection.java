package com.itz_mine.mark_i;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class CheckNetworkConnection {

	// test urls

	public String hosturl = "http://test.foodiesbuddy.com/mobile/pizza3/getdetails.php?";
	public String thumbimagehosturl = "http://test.foodiesbuddy.com/upload/images/thumbs/thumb_";
	public String normalimagehosturl = "http://test.foodiesbuddy.com/";

	
	public static boolean isConnectionAvailable(Context context) {

		Log.d("#### "+Utils.getLogMsgId(), "Checking connection availability...");
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivityManager != null) {
			Log.d ("#### "+Utils.getLogMsgId(), "Checking...");
			NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
			if (netInfo != null && netInfo.isConnected()
					&& netInfo.isConnectedOrConnecting()
					&& netInfo.isAvailable()) {
				Log.d("#### "+Utils.getLogMsgId(), "Connection Available. Returning true");
				return true;
			}
		}
		Log.d("#### "+Utils.getLogMsgId(), "Connection Unavailable. Returning false");
		return false;
	}
}
