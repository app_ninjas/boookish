package com.itz_mine.mark_i;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.app.Fragment;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.pkmmte.view.CircularImageView;

import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfileFragment extends android.support.v4.app.Fragment {

    user_creds user;

    public MyProfileFragment() {
        // Required empty public constructor
    }

    ImageView blurredImageView;
    CircularImageView circularImageView;
    TextView walletbutton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Date dob;
        int age;
        View fragmentView =  inflater.inflate(R.layout.fragment_my_profile, container, false);
        TextView txtuser = (TextView) fragmentView.findViewById(R.id.welcome_textview);
        TextView phn_num = (TextView) fragmentView.findViewById(R.id.phn_num);

        blurredImageView = (ImageView) fragmentView.findViewById(R.id.blurred_bg);
        circularImageView = (CircularImageView)fragmentView.findViewById(R.id.circularImageView);
        walletbutton = (TextView) fragmentView.findViewById(R.id.wallet_button);

        Bitmap bg_image;
        ProgressBar imageUploadProgressBar = (ProgressBar) fragmentView.findViewById(R.id.imgUploadProgressBar);
        imageUploadProgressBar.setVisibility(View.INVISIBLE);

        Bundle userdata = this.getArguments();
        if (userdata == null) {
            Log.e("#### "+Utils.getLogMsgId(), "EMPTY BUNDLE RECEIVED");
        } else
            Log.d("#### "+Utils.getLogMsgId(), "BUNDLE IS NOT EMPTY");

        user = (user_creds) userdata.getSerializable("user");
        if (user == null) {
            Log.e ("#### "+Utils.getLogMsgId(), "GOT NULL user_creds FROM BUNDLE");
        } else
            Log.d ("#### "+Utils.getLogMsgId(), "Got data from bundle: "+user.getFull_name());

        setProfImages(user.getProfile_pic());

        txtuser.setText(user.getFull_name());
        String phone_number = user.getMobile_number();
        if (phone_number != null)
            phn_num.setText(phone_number);

        updateWallet(user.getWallet());

        return fragmentView;
    }

    public void updateWallet (int wallet) {
        String rupee_symbol = getResources().getString(R.string.Rs);
        walletbutton.setText("Wallet :  "+rupee_symbol+" "+wallet+" /-");
    }

    public void setProfImages (byte[] profile_pic) {
        Bitmap blurredBitmap = createBlurredImage(profile_pic);
        Bitmap originalBitmap;

        Drawable blurredbg = new BitmapDrawable(getResources(), blurredBitmap);

        blurredImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        blurredImageView.getBackground().setColorFilter(Color.BLACK, PorterDuff.Mode.DARKEN);
        blurredImageView.setAlpha(150);
        blurredImageView.setImageDrawable(blurredbg);

        if (profile_pic == null) {
            Log.e("#### "+Utils.getLogMsgId(), "profile pic is null");
            originalBitmap = BitmapFactory.decodeResource (getResources(), R.drawable.profilepiclarge);
        } else {
            originalBitmap = BitmapFactory.decodeByteArray(profile_pic, 0, profile_pic.length);
        }

        Drawable circularDrawable = new BitmapDrawable(getResources(), originalBitmap);
        circularImageView.addShadow();
        circularImageView.setImageDrawable(circularDrawable);
    }

    private Bitmap createBlurredImage (byte[] profile_pic) {
        // Load a clean bitmap and work from that.
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;
        Bitmap originalBitmap;
        if (profile_pic == null) {
            Log.e("#### "+Utils.getLogMsgId(), "profile pic is null");
            originalBitmap = BitmapFactory.decodeResource (getResources(), R.drawable.profilepiclarge, options);
        }
        else {
            originalBitmap = BitmapFactory.decodeByteArray(profile_pic, 0, profile_pic.length, options);
        }

        // Create another bitmap that will hold the results of the filter.
        Bitmap blurredBitmap;
        blurredBitmap = Bitmap.createBitmap (originalBitmap);

        // Create the Renderscript instance that will do the work.
        RenderScript rs = RenderScript.create(getActivity());

        // Allocate memory for Renderscript to work with
        Allocation input = Allocation.createFromBitmap (rs, originalBitmap, Allocation.MipmapControl.MIPMAP_FULL, Allocation.USAGE_SCRIPT);
        Allocation output = Allocation.createTyped(rs, input.getType());

        // Load up an instance of the specific script that we want to use.
        ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create (rs, Element.U8_4 (rs));
        script.setInput(input);

        // Set the blur radius
        script.setRadius (25);

        // Start the ScriptIntrinisicBlur
        script.forEach(output);

        // Copy the output to the blurred bitmap
        output.copyTo (blurredBitmap);

        return blurredBitmap;
    }

}
