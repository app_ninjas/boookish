package com.itz_mine.mark_i;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import android.content.Intent;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.itz_mine.mark_i.R.drawable.harry;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyBooksFragment extends Fragment {

    private ListView myBooks;
    private List<String> myBooksList;
    user_creds user;
    ArrayList<books> userbooks;
    private static final String ACTION_FOR_INTENT_CALLBACK = "THIS_IS_A_UNIQUE_KEY_WE_USE_TO_COMMUNICATE";
    View fragmentView;

    public MyBooksFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView =  inflater.inflate(R.layout.fragment_my_books, container, false);

        Bundle userdata = this.getArguments();
        updateBookShelf(userdata);

        return fragmentView;
    }

    public void updateBookShelf (Bundle userdata) {
        int num_of_books = 0;

        if (userdata == null) {
            Log.e("#### "+Utils.getLogMsgId(), "EMPTY BUNDLE RECEIVED");
        } else
            Log.d("#### "+Utils.getLogMsgId(), "BUNDLE IS NOT EMPTY");

        user = (user_creds) userdata.getSerializable("user");
        if (user == null) {
            Log.e ("#### "+Utils.getLogMsgId(), "GOT NULL user_creds FROM BUNDLE");
        } else
            Log.d("#### "+Utils.getLogMsgId(), "Got data from bundle: " + user.getFull_name());

        userbooks = user.getUserbooks();

        if (userbooks != null) {
            num_of_books = userbooks.size();
            Log.d("#### "+Utils.getLogMsgId(), "num_of_books = "+num_of_books);
        }

        myBooks = (ListView) fragmentView.findViewById(R.id.mybookslistView);
        myBooks.invalidateViews();
        myBooksList = new ArrayList<String>();

        myBooksList.add("1");
        myBooksList.add("2");
        myBooksList.add("3");
        myBooksList.add("4");

        myBooks.setAdapter(new myAdaptor(num_of_books));
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }


    private class myAdaptor extends BaseAdapter{

        int count;
        int num_of_books;

        public myAdaptor(int book_num) {
            count = 0;
            num_of_books = book_num;
        }

        @Override
        public int getCount() {
            return myBooksList.size();
        }

        @Override
        public Object getItem(int position) {
            return myBooksList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Because the view gets created everytime we scroll up or down, the count has
            // to be drawn dynamically too.
            count = position * 3;
            String image_url = null;
            View rowview = getActivity().getLayoutInflater().inflate(R.layout.bookshelf,null);

            Log.d ("#### "+Utils.getLogMsgId(), "count = "+count+" position = "+position);

            if (count > num_of_books)
                return rowview;

            ImageView book1 = (ImageView)rowview.findViewById(R.id.bookimage1);
            ImageView book2 = (ImageView)rowview.findViewById(R.id.bookimage2);
            ImageView book3 = (ImageView)rowview.findViewById(R.id.bookimage3);

            if (count == num_of_books) {
                book1.setBackgroundResource(R.drawable.add_book);
                book1.setOnClickListener(add_book_on_click_listner);
                book1.setTag(++count);
                return rowview;
            } else {
                image_url = userbooks.get(count).getImageLinks();
                if (image_url != null) {
                    Log.d("#### "+Utils.getLogMsgId(), "image_url = "+image_url);
                    Picasso.with(getActivity()).load(image_url)
                            .into(book1);
                } else {
                    book1.setImageResource(R.drawable.no_book_cover);
                }
                book1.setTag(++count);
            }

            if (count == num_of_books) {
                book2.setBackgroundResource(R.drawable.add_book);
                book2.setOnClickListener(add_book_on_click_listner);
                book2.setTag(++count);
                return rowview;
            } else {
                image_url = userbooks.get(count).getImageLinks();
                if (image_url != null) {
                    Log.d("#### "+Utils.getLogMsgId(), "image_url = "+image_url);
                    Picasso.with(getActivity()).load(image_url)
                            .into(book2);
                } else {
                    book2.setImageResource(R.drawable.no_book_cover);
                }
                book2.setTag(++count);
            }

            if (count == num_of_books) {
                book3.setBackgroundResource(R.drawable.add_book);
                book3.setOnClickListener(add_book_on_click_listner);
                book3.setTag(++count);
                return rowview;
            } else {
                image_url = userbooks.get(count).getImageLinks();
                if (image_url != null) {
                    Log.d("#### "+Utils.getLogMsgId(), "image_url = "+image_url);
                    Picasso.with(getActivity()).load(image_url)
                            .into(book3);
                } else {
                    book3.setImageResource(R.drawable.no_book_cover);
                }
                book3.setTag(++count);
            }

            return rowview;
        }

        private View.OnClickListener add_book_on_click_listner = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (Integer) v.getTag();
                Log.d("#### "+Utils.getLogMsgId(), "Clicked add book icon at position " + position);
                ((WelcomeActivity)getActivity()).goToAddBooks(v);
            }
        };
    }
}
