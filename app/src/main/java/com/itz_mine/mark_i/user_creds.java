package com.itz_mine.mark_i;

import android.graphics.Bitmap;

import com.parse.Parse;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by root on 8/19/15.
 */
public class user_creds implements java.io.Serializable {
    private String user_id;
    private String full_name;
    private byte[] profile_pic;
    private Date DOB;
    private String mobile_number;
    private  String gender;
    private ParseGeoPoint point;
    private String address;
    private ArrayList<String> book_isbns;
    private ArrayList<books> userbooks;
    private ArrayList<String> blocked_user_ids;
    private String msg;
    private String profilePicUrl;

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ArrayList<String> getBlocked_user_ids() {
        return blocked_user_ids;
    }

    public void setBlocked_user_ids(ArrayList<String> blocked_user_ids) {
        this.blocked_user_ids = blocked_user_ids;
    }

    public String getChannel_url() {
        return channel_url;
    }

    public void setChannel_url(String channel_url) {
        this.channel_url = channel_url;
    }

    private String channel_url;

    public int getWallet() {
        return wallet;
    }

    public void setWallet(int wallet) {
        this.wallet = wallet;
    }

    private int wallet;

    public ParseFile getProfilePhotoFile() {
        return profilePhotoFile;
    }

    public void setProfilePhotoFile(ParseFile profilePhotoFile) {
        this.profilePhotoFile = profilePhotoFile;
    }

    ParseFile profilePhotoFile;


    public ArrayList<books> getUserbooks() {
        return userbooks;
    }

    public void setUserbooks(ArrayList<books> userbooks) {
        this.userbooks = userbooks;
    }

    public ArrayList<String> getBook_isbns() {
        return book_isbns;
    }

    public void setBook_isbns(ArrayList<String> book_isbns) {
        this.book_isbns = book_isbns;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public byte[] getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(byte[] profile_pic) {
        this.profile_pic = profile_pic;
    }

    public Date getDOB() {
        return DOB;
    }

    public void setDOB(Date DOB) {
        this.DOB = DOB;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getGender() {
        return gender;
    }

    public void setGender (String gender) {
        this.gender = gender;
    }

    public ParseGeoPoint getPoint() {
        return point;
    }

    public void setPoint(ParseGeoPoint point) {
        this.point = point;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress (String address) {
        this.address = address;
    }

}
