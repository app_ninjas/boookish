package com.itz_mine.mark_i;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.facebook.FacebookSdk;
import com.parse.Parse;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;
import android.net.ConnectivityManager;
import android.content.Context;
import android.net.NetworkInfo;
import android.util.Log;


public class MainActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        seekSession();

        /*
        if(isNetworkAvailable(this) == false) {
            AlertDialog alertDialog = new AlertDialog.Builder(this,AlertDialog.THEME_HOLO_DARK).create();

            alertDialog.setTitle("Info");
            alertDialog.setMessage("Internet or GPS not available, Please enable and try again.");
            alertDialog.setIcon(android.R.drawable.ic_dialog_alert);

            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    System.exit(0);
                }
            });

            alertDialog.show();
        }
        else {
            seekSession();
        }
        */
    }

    public static boolean isNetworkAvailable(Context context)
    {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean networkenable,gpsenable;
        networkenable = false;
        gpsenable = false;
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();

            if (info != null)
            {
                for (int i = 0; i < info.length; i++)
                {
                    Log.i("#### "+Utils.getLogMsgId(), "Class: "+info[i].getState().toString());
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        networkenable = true;
                    }
                }
            }
        }

        LocationManager mlocManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);;
        gpsenable = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if(networkenable = true && gpsenable == true)
        {
            return true;
        }
        return false;
    }

    public void seekSession () {
        // Determine whether the current user is an anonymous user
        ParseUser currentUser = ParseUser.getCurrentUser();
        if ((currentUser == null) || (ParseAnonymousUtils.isLinked(currentUser))) {
            // If user is anonymous, send the user to LoginSignupActivity.class
            goToUserLoginActivity();
        } else {
            // If current user is NOT anonymous user
            // Get current user data from Parse.com
            if (currentUser != null) {
                // Send logged in users to Welcome.class
                goToWelcomeActivity();
            } else {
                // Send user to LoginSignupActivity.class
                goToUserLoginActivity();
            }
        }
    }

    public void goToUserLoginActivity() {
        Intent loginIntent = new Intent(this, Login.class);
        startActivity(loginIntent);
        finish();
    }

    public void goToWelcomeActivity() {
        Intent welcomeIntent = new Intent(this, WelcomeActivity.class);
        startActivity(welcomeIntent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
    }
}
