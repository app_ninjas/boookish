package com.itz_mine.mark_i;


import com.parse.ParseFacebookUtils;

import android.support.v7.app.ActionBarActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.facebook.internal.ImageRequest;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import android.content.SharedPreferences;

public class MobileVerifyActivity extends ActionBarActivity {
	Button test;
	EditText mobilenum;
	EditText countrycode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d("#### "+Utils.getLogMsgId(), "inside mobileverifyactivity");
		super.onCreate(savedInstanceState);
		Log.d("#### "+Utils.getLogMsgId(), "inside mobileverifyactivity-1");
		setContentView(R.layout.activity_verify);
		Log.d("#### "+Utils.getLogMsgId(), "inside mobileverifyactivity-2");
		test = (Button) findViewById(R.id.test);
		mobilenum = (EditText) findViewById(R.id.editText1);
		countrycode = (EditText) findViewById(R.id.editText2);
		//countrycode.setText(VerifyMobile
		//		.getCountryCode(getApplicationContext()));
		test.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String mobile = countrycode.getText().toString()
						+ mobilenum.getText().toString();
				Log.d("#### "+Utils.getLogMsgId(), "inside mobileverifyactivity-3");
				Intent in = new Intent(MobileVerifyActivity.this, VerifyMobile.class);

				in.putExtra("app_id", "801ee2bc85784dd9b850f2e");
				in.putExtra("access_token",
						"e5a375e2d0eb7bc32eaf798ecf3122a723cc2e3b");
				in.putExtra("mobile", mobile);
				if (mobile.length() == 0) {
					countrycode.setError("Please enter mobile number");
				} else {
					if (CheckNetworkConnection
							.isConnectionAvailable(getApplicationContext())) {
						Log.d("#### "+Utils.getLogMsgId(), "inside ");
						startActivityForResult(in, VerifyMobile.REQUEST_CODE);
					} else {
						Toast.makeText(getApplicationContext(),
								"no internet connection", Toast.LENGTH_SHORT)
								.show();
					}
				}

			}
		});
	}

	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		// TODO Auto-generated method stub
		super.onActivityResult(arg0, arg1, arg2);

		if (arg0 == VerifyMobile.REQUEST_CODE) {
			String message = arg2.getStringExtra("message");
			int result = arg2.getIntExtra("result", 0);

			Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT)
					.show();
			Toast.makeText(getApplicationContext(), "" + result,
					Toast.LENGTH_SHORT).show();

		}
	}

}
