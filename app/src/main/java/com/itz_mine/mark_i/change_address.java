package com.itz_mine.mark_i;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.SearchView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Locale;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.logging.Handler;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 */
public class change_address extends Fragment {
    MapView mMapView;
    private GoogleMap googleMap;
    public SeekBar seekbar;
    private double latitude;
    private double longitude;
    public int circle_size = 0;
    public double distance;
    public TextView addressview;
    Button locationbutton;
    SearchView search;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflate and return the layout
        View v = inflater.inflate(R.layout.change_address_fragment, container,
                false);
        search = (SearchView) v.findViewById(R.id.searchView);

        addressview = (TextView) v.findViewById(R.id.addressview);
        //locationbutton = (Button) v.findViewById(R.id.location_button);
        locationbutton = (Button) v.findViewById(R.id.location_button);
        locationbutton.setText("CONFIRM LOCATION");
        addressview.setText("Select Marker at your location and then press CONFIRM LOCATION Button");

        mMapView = (MapView) v.findViewById(R.id.mapView1);
        mMapView.onCreate(savedInstanceState);

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                _onMapReady(googleMap);
            }
        });

        return v;
    }

    public void _onMapReady(final GoogleMap map) {

        googleMap = map;
        Context act_cntxt = getActivity();
        if (ActivityCompat.checkSelfPermission(act_cntxt, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(act_cntxt, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        MarkerOptions markerOptions = new MarkerOptions();
        GPSTracker gps = new GPSTracker(getActivity());
        gps.getLocation();
        latitude = gps.latitude;
        longitude = gps.longitude;
        markerOptions.position(new LatLng(latitude, longitude));
        markerOptions.draggable(true);
        // markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.logo));
        markerOptions.title("Current Location");
        googleMap.addMarker(markerOptions);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15));
        googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker markerDragStart) {
                // TODO Auto-generated method stub
                Log.e("#### "+Utils.getLogMsgId(), "Marker Start ");
            }

            @Override
            public void onMarkerDragEnd(Marker markerDragEnd) {
                Log.e("#### "+Utils.getLogMsgId(), "Marker End ");

                latitude = markerDragEnd.getPosition().latitude;
                longitude = markerDragEnd.getPosition().longitude;
                Log.e("#### "+Utils.getLogMsgId(), String.valueOf(latitude));
                Log.e("#### "+Utils.getLogMsgId(), String.valueOf(latitude));

            }

            @Override
            public void onMarkerDrag(Marker markerDrag) {

                Log.e("#### "+Utils.getLogMsgId(), "Marker ");
            }
        });

        // googleMap.setOnMarkerDragListener((GoogleMap.OnMarkerDragListener) getActivity());

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                MarkerOptions marker = new MarkerOptions().position(
                        new LatLng(latLng.latitude, latLng.longitude)).title("New Marker");
                latitude = latLng.latitude;
                longitude = latLng.longitude;
                googleMap.clear();
                googleMap.addMarker(marker);
            }
        });



        locationbutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                StringBuilder strAddress = new StringBuilder();
                Log.d("#### "+Utils.getLogMsgId(), "before geocoder");
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                try {
                    Log.d("#### "+Utils.getLogMsgId(), "inside try");
                    Log.d("####"+Utils.getLogMsgId(), String.valueOf(latitude));
                    Log.d("#### "+Utils.getLogMsgId(), String.valueOf(longitude));

                    //Place your latitude and longitude
                    List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 2);

                    if (addresses != null) {
                        Log.d("#### "+Utils.getLogMsgId(), "inside address");
                        Address fetchedAddress = addresses.get(0);


                        for (int i = 0; i < fetchedAddress.getMaxAddressLineIndex(); i++) {
                            strAddress.append(fetchedAddress.getAddressLine(i)).append("\n");
                        }
                        Log.d("#### "+Utils.getLogMsgId(), strAddress.toString());
                        addressview.setText("I am at: " + strAddress.toString());

                        ((WelcomeActivity) getActivity()).saveaddress(latitude, longitude, strAddress.toString());
                        strAddress = null;
                        locationbutton.setText("LOCATION CONFIRMED");
                    } else
                        addressview.setText("No location found..!");

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Could not get address..!", Toast.LENGTH_LONG).show();
                }

            }
        });


        //*** setOnQueryTextFocusChangeListener ***


        search.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub

                //Toast.makeText(getActivity(), String.valueOf(hasFocus),Toast.LENGTH_SHORT).show();
            }
        });

        //*** setOnQueryTextListener ***
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                // TODO Auto-generated method stub
                Log.d("#### "+Utils.getLogMsgId(), "INSIDE onQueryTextSubmit");
                //Toast.makeText(getActivity(), query,Toast.LENGTH_SHORT).show();



                String g = search.getQuery().toString();

                Geocoder geocoder = new Geocoder(getActivity());
                List<Address> addresses = null;

                try {
                    // Getting a maximum of 3 Address that matches the input
                    // text
                    addresses = geocoder.getFromLocationName(g, 3);
                    if (addresses != null && !addresses.equals(""))
                        search(addresses);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Could not get address..!", Toast.LENGTH_LONG).show();
                }

                return false;
            }


            @Override
            public boolean onQueryTextChange(String newText) {
                // TODO Auto-generated method stub

                //Toast.makeText(getActivity(), newText,Toast.LENGTH_SHORT).show();
                return false;
            }
        });
    }

    protected void search(List<Address> addresses) {

        Address address = (Address) addresses.get(0);
        double home_long = address.getLongitude();
        double home_lat = address.getLatitude();
        LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());

        String addressText = String.format(
                "%s, %s",
                address.getMaxAddressLineIndex() > 0 ? address
                        .getAddressLine(0) : "", address.getCountryName());

        MarkerOptions markerOptions = new MarkerOptions();

        markerOptions.position(latLng);
        markerOptions.title(addressText);

        googleMap.clear();
        googleMap.addMarker(markerOptions);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));


    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }


}