package com.itz_mine.mark_i;

/**
 * Created by root on 12/1/16.
 */
public class Utils {
    /*
     * Get the current fine name, method name and line number.
     */
    public static String getLogMsgId() {
        String msg_id = Thread.currentThread().getStackTrace()[3].getFileName()+
                        " : "+Thread.currentThread().getStackTrace()[3].getMethodName()+
                        " : "+Thread.currentThread().getStackTrace()[3].getLineNumber()+
                        " : ";
        return msg_id;
    }
}
