package com.itz_mine.mark_i;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.facebook.internal.ImageRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.matesnetwork.Cognalys.VerifyMobile;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.StringTokenizer;


public class WelcomeActivity extends ActionBarActivity {

    user_creds user;
    SearchFragment searchFragment;
    MyBooksFragment myBooksFragment;
    MyProfileFragment myProfileFragment;
    editProfile editProfileFragment;
    mobile_verification mobileVerificationFragment;
    add_book_fragment addBookFragment;
    wallet_fragment walletFragment;
    chatFragment chatfragment;
    chat_with_user chatWithUserFragment;
    change_address addressFragment;
    viewUserFragment view_User_Fragment;
    ArrayList<books> users_books;
    ViewPager viewPager;
    Bundle userdata;
    Boolean isPicPresent;
    public int mobverify = 0;
    public ParseGeoPoint parseGeoPoint;
    public static String chatuser;
    private ListView picOpsListView;
    private List<String> picOpsList;
    Boolean getting_user_stat_complete;
    Boolean getting_isbn_data_complete;
    Boolean getting_sendbird_conn;
    Boolean show_started;
    Object starting_show_mutex;
    String noImageurl;

    private final static int REQUEST_SELECT_IMAGE = 12345;
    private final static int REQUEST_VERIFY_MOBILE = 969;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        setBoeiLogo();

        isPicPresent = false;
        userdata = new Bundle();
        user = new user_creds();
        viewPager = (ViewPager) findViewById(R.id.pager);
        searchFragment = new SearchFragment();
        myBooksFragment = new MyBooksFragment();
        myProfileFragment = new MyProfileFragment();
        editProfileFragment = new editProfile();
        mobileVerificationFragment = new mobile_verification();
        addBookFragment = new add_book_fragment();
        chatfragment = new chatFragment();
        chatWithUserFragment = new chat_with_user();
        addressFragment = new change_address();
        view_User_Fragment = new viewUserFragment();
        walletFragment = new wallet_fragment();
        getting_user_stat_complete = false;
        getting_isbn_data_complete = false;
        getting_sendbird_conn = false;
        show_started = false;
        starting_show_mutex = new Object();
        noImageurl = "https://3.bp.blogspot.com/-BAIc4kH2ark/WEAECmdiiAI/AAAAAAAAAS4/F4aeaqvX4kAMqM33sxIFj614bDBznQcMACLcB/s1600/profilepicsmall.png";

        Log.d("#### " + Utils.getLogMsgId(), "In Welcome Activity");

        getCurrentUser();
    }

    public void setBoeiLogo () {
        TextView bookesh_logo = (TextView) findViewById(R.id.boei_logo);
        Typeface blackJack_tf = Typeface.createFromAsset(getAssets(), "fonts/black_jack.ttf");
        bookesh_logo.setTypeface(blackJack_tf);
    }

    public class MyAdapter extends FragmentPagerAdapter implements PagerSlidingTabStrip.IconTabProvider {
        private int curenttab;
        private int icons[] = {R.drawable.tabsearchicon, R.drawable.ic_action_chat_icon, R.drawable.tabprofileicon};

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            curenttab = position;

            if (position == 0) {
                return searchFragment;
            } else if (position == 1) {
                return chatfragment;
            } else if (position == 2) {
                return myProfileFragment;
            } else
                return null;
        }

        @Override
        public int getCount() {
            return 3    ;
        }

        @Override
        public int getPageIconResId(int i) {
            return icons[i];
        }
    }

    public void saveImageInParse (Intent data) {
        Uri selectedImage = data.getData();

        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        final ProgressBar imageUploadProgressBar = (ProgressBar) findViewById(R.id.imgUploadProgressBar);
        imageUploadProgressBar.setVisibility(View.VISIBLE);

        Cursor cursor = getContentResolver().query(
                selectedImage, filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String filePath = cursor.getString(columnIndex);
        cursor.close();


        Bitmap imageBitmap = BitmapFactory.decodeFile(filePath);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        final byte[] image = stream.toByteArray();

        user.setProfile_pic(image);
        userdata.putSerializable("user", user);
        final ParseFile file = new ParseFile("profilePicture.png", image);
        file.saveInBackground();

        final ParseObject user_creds = new ParseObject("user_creds");
        ParseUser currentUser = ParseUser.getCurrentUser();
        String user_id = currentUser.get("user_id").toString();
        Log.d("#### "+ Utils.getLogMsgId(), user_id);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("user_creds");
        query.whereEqualTo("user_id", user_id);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (object == null) {
                    Log.d("#### "+ Utils.getLogMsgId(), "The getFirst request failed.");
                } else {
                    object.remove("profile_photo");
                    object.put("profile_photo", file);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Log.d("#### "+ Utils.getLogMsgId(), "Photo uploaded successfully");
                                isPicPresent = true;
                                user.setProfilePicUrl(file.getUrl());
                                updateSendbirdUserInfo();
                            } else {
                                Log.d("#### "+ Utils.getLogMsgId(), "Error: "+e.getMessage());
                            }
                            imageUploadProgressBar.setVisibility(View.INVISIBLE);
                            refreshProfileFragment(image);
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == REQUEST_SELECT_IMAGE) && resultCode == RESULT_OK) {
            saveImageInParse(data);
        }
    }

    public void fetchUserData(final String user_id) {
        /* Fetch user info from user_creds db */
        ParseQuery query = new ParseQuery("user_creds");
        Log.d("#### "+ Utils.getLogMsgId(), "user name: "+user_id);
        query.whereEqualTo("user_id", user_id);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                Log.d("#### " + Utils.getLogMsgId(), "Done");
                if (e == null) {
                    Log.d("#### " + Utils.getLogMsgId(), "TEST");
                    for (ParseObject object : list) {
                        user.setUser_id(user_id);
                        user.setFull_name((String) object.get("full_name"));
                        Log.d("#### " + Utils.getLogMsgId(), object.toString());
                        Log.d("#### " + Utils.getLogMsgId(), user.getFull_name());
                        user.setDOB((Date) object.get("DOB"));
                        user.setGender((String) object.get("gender"));
                        user.setMobile_number((String) object.get("mobile_number"));
                        user.setBook_isbns((ArrayList<String>) object.get("book_isbns"));
                        user.setBlocked_user_ids((ArrayList<String>) object.get("blocked_list"));
                        if (object.get("Wallet") != null)
                            user.setWallet((int) object.get("Wallet"));
                        else
                            user.setWallet(0);
                        //user.setPoint((ParseGeoPoint) object.get("location"));
                        user.setAddress((String) object.get("address"));
                        ParseFile file = (ParseFile) object.get("profile_photo");
                        if (file != null) {
                            user.setProfilePicUrl(file.getUrl());
                            file.getDataInBackground(new GetDataCallback() {
                                @Override
                                public void done(byte[] bytes, ParseException e) {
                                    if (e == null) {
                                        user.setProfile_pic(bytes);
                                        isPicPresent = true;
                                        // data has the bytes for the image
                                    } else {
                                        Log.e("#### " + Utils.getLogMsgId(), "Error:" + e.getMessage());
                                    }
                                    synchronized (starting_show_mutex) {
                                        getting_user_stat_complete = true;
                                    }
                                    startTheShow();
                                }
                            });
                        } else {
                            Log.d("#### " + Utils.getLogMsgId(), "file is null");
                            user.setProfilePicUrl(noImageurl);
                            synchronized (starting_show_mutex) {
                                getting_user_stat_complete = true;
                            }
                            startTheShow();
                        }
                        fetchIsbnData();
                    }
                } else {
                    Log.e("#### " + Utils.getLogMsgId(), "Error : " + e.getMessage());
                }
            }
        });
    }

    public void getCurrentUser () {
        ParseUser currentUser = ParseUser.getCurrentUser();

        final String user_id = currentUser.get("user_id").toString();

        Log.d("#### " + Utils.getLogMsgId(), "Registering with sendbird...");

        SendBird.connect(user_id, new SendBird.ConnectHandler() {
            @Override
            public void onConnected(User user, SendBirdException e) {
                if (e == null) {
                    Log.d("#### " + Utils.getLogMsgId(), "Successfully connected to sendbird with user id: " + user_id);

                    if (FirebaseInstanceId.getInstance().getToken() == null) {
                        Log.e("#### " + Utils.getLogMsgId(), "Failed to get firebase token for push notifications");
                    } else {
                        SendBird.registerPushTokenForCurrentUser(FirebaseInstanceId.getInstance().getToken(), new SendBird.RegisterPushTokenWithStatusHandler() {
                            @Override
                            public void onRegistered(SendBird.PushTokenRegistrationStatus pushTokenRegistrationStatus, SendBirdException e) {
                                if (e == null) {
                                    Log.d("#### " + Utils.getLogMsgId(), "Successfully registered with sendbird for push notification");
                                } else {
                                    Log.e("#### " + Utils.getLogMsgId(), "Failed to register sendbird with push token. Error: " + e.getMessage());
                                    return;
                                }
                            }
                        });
                    }
                    synchronized (starting_show_mutex) {
                        getting_sendbird_conn = true;
                    }
                    startTheShow();
                } else {
                    Log.e("#### " + Utils.getLogMsgId(), "Failed to connect to sendbird with user_id: " + user_id + "Error: " + e.getMessage());
                }
            }
        });
        fetchUserData(user_id);
    }

    public void onLogOutClick (View v) throws IOException {
        SendBird.unregisterPushTokenForCurrentUser(FirebaseInstanceId.getInstance().getToken(), new SendBird.UnregisterPushTokenHandler() {
            @Override
            public void onUnregistered(SendBirdException e) {
                if (e == null) {
                    Log.d("#### " + Utils.getLogMsgId(), "Successfully unregistered with sendbird for push notification");
                } else
                    Log.e("#### " + Utils.getLogMsgId(), "Failed to unnregister sendbird push notification. Error: " + e.getMessage());

                ParseUser.logOut();
                goToUserLoginActivity();
            }
        });
    }

    public void goToUserLoginActivity() {
        Intent loginIntent = new Intent(this, Login.class);
        startActivity(loginIntent);
        finish();
    }

    public void chatwithuser(Bundle userdata){
        Log.d("#### ", "Inside chatwithuser");
        viewPager.setCurrentItem(1);
        call_chat_user_fragment(userdata);
    }

    public void call_chat_user_fragment (Bundle userdata) {
        chatWithUserFragment.setArguments(userdata);
        this.getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.slide_in_left, R.animator.slide_out_left,
                        R.animator.slide_out_right, R.animator.slide_in_right)
                .replace(R.id.chat_console_id, chatWithUserFragment, null)
                .addToBackStack(null)
                .commit();
    }

    public void gotoverifymobile(View V) {
        Log.d("#### " + Utils.getLogMsgId(), "inside gotoverifymobile");
        this.getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.slide_in_left, R.animator.slide_out_left,
                        R.animator.slide_out_right, R.animator.slide_in_right)
                .replace(R.id.edit_profile_fragment_id, mobileVerificationFragment, null)
                .addToBackStack(null)
                .commit();
    }

    public void onClickPickPhoto(View view) {
        Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, REQUEST_SELECT_IMAGE);
    }

    public void updateSendbirdUserInfo () {
        SendBird.updateCurrentUserInfo(user.getFull_name(), user.getProfilePicUrl(), new SendBird.UserInfoUpdateHandler() {
            @Override
            public void onUpdated(SendBirdException e) {
                if (e == null) {
                    Log.d("#### " + Utils.getLogMsgId(), "Successfully updated nick name, and profile pic details in sendbird for user_id: " + user.getUser_id());
                } else {
                    // Error.
                    Log.e("#### " + Utils.getLogMsgId(), "Failed to update nick name and profile pic details in sendbird with user_id: " + user.getUser_id() + " Error: " + e.getMessage());
                }
            }
        });
    }

    public void onClickRemovePhoto (View view) {
        final ProgressBar imageUploadProgressBar = (ProgressBar) findViewById(R.id.imgUploadProgressBar);
        imageUploadProgressBar.setVisibility(View.VISIBLE);
        final ParseObject user_creds = new ParseObject("user_creds");
        ParseUser currentUser = ParseUser.getCurrentUser();
        String user_id = currentUser.get("user_id").toString();
        Log.d("#### "+ Utils.getLogMsgId(), user_id);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("user_creds");
        query.whereEqualTo("user_id", user_id);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (object == null) {
                    Log.d("#### " + Utils.getLogMsgId(), "The getFirst request failed.");
                } else {
                    object.remove("profile_photo");
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Log.d("#### " + Utils.getLogMsgId(), "Photo removed successfully");
                                isPicPresent = false;
                                user.setProfile_pic(null);
                                user.setProfilePicUrl(noImageurl);
                                userdata.putSerializable("user", user);
                                refreshProfileFragment(null);
                                updateSendbirdUserInfo();
                            } else {
                                Log.e("#### " + Utils.getLogMsgId(), "Error : " + e.getMessage());
                            }
                            imageUploadProgressBar.setVisibility(View.INVISIBLE);
                        }
                    });
                }
            }
        });
    }

    public void refreshProfileFragment (byte[] profile_photo){
        myProfileFragment.setProfImages(profile_photo);
        if (editProfileFragment.getActivity() != null)
            editProfileFragment.setProfImages(profile_photo);
        else
            editProfileFragment.setArguments(userdata);
    }

    public int getAge(Calendar born) {
        int age = 0;
        Calendar now = Calendar.getInstance();
        if(born!= null) {
            now.setTime(new Date());
            if(born.after(now)) {
                final AlertDialog alertDialog = new AlertDialog.Builder(this,AlertDialog.THEME_HOLO_DARK).create();

                alertDialog.setTitle("Invalid Date Of Birth");
                alertDialog.setMessage("Date Of Birth Cannot Be Greater Than Current Date");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);

                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.cancel();
                    }
                });

                alertDialog.show();
            }
            age = now.get(Calendar.YEAR) - born.get(Calendar.YEAR);
            if(now.get(Calendar.DAY_OF_YEAR) < born.get(Calendar.DAY_OF_YEAR))  {
                age-=1;
            }
        }
        return age;
    }

    public void changeDOB (View v) {
        TextView ageTextView = (TextView) findViewById(R.id.dob_button);
        ageTextView.setBackgroundColor(getResources().getColor(R.color.clicked_Button_color));

        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }



    public void changeaddress (View v) {
        Log.d("#### " + Utils.getLogMsgId(), "inside change address");
        this.getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.slide_in_left, R.animator.slide_out_left,
                        R.animator.slide_out_right, R.animator.slide_in_right)
                .replace(R.id.edit_profile_fragment_id, addressFragment, null)
                .addToBackStack(null)
                .commit();
    }

    public void add_money_to_wallet (View v) {
        final EditText add_money_amount_edittext = (EditText) findViewById(R.id.add_to_wallet_amount);
        final int add_money_amount = Integer.parseInt(add_money_amount_edittext.getText().toString());
        final TextView add_money_status = (TextView) findViewById(R.id.add_money_status_display);
        final String rupee_symbol = getResources().getString(R.string.Rs);
        int current_wallet_amount = user.getWallet();

        if (add_money_amount <= 0) {
            add_money_status.setText("Please enter a valid amount.");
            return;
        }

        final int new_wallet_amount = current_wallet_amount + add_money_amount;

        ParseUser currentUser = ParseUser.getCurrentUser();
        String user_id = currentUser.get("user_id").toString();
        Log.d("#### " + Utils.getLogMsgId(), user_id);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("user_creds");
        query.whereEqualTo("user_id", user_id);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (object == null) {
                    Log.d("#### ", "The getFirst request failed.");
                } else {
                    object.remove("Wallet");
                    object.put("Wallet", new_wallet_amount);
                    user.setWallet(new_wallet_amount);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Log.d("#### " + Utils.getLogMsgId(), "Wallet updated successfully");
                                userdata.putSerializable("user", user);
                                myProfileFragment.updateWallet(new_wallet_amount);
                                walletFragment.updateWallet(new_wallet_amount);
                                add_money_status.setVisibility(View.VISIBLE);
                                add_money_status.setText(rupee_symbol + " " + add_money_amount + "\\- added successfully");
                                add_money_amount_edittext.setText("");
                            } else {
                                Log.e("#### " + Utils.getLogMsgId(), "Error : " + e.getMessage());
                                add_money_status.setVisibility(View.VISIBLE);
                                add_money_status.setText("Failed to add " + rupee_symbol + " " + add_money_amount + "\\-");
                                add_money_amount_edittext.setText("");
                            }
                        }
                    });
                }
            }
        });
    }


    public void changegender (View v) {
        final String genderinp,genderset;
        TextView genderTextView = (TextView) findViewById(R.id.gender_button);
        genderTextView.setBackgroundColor(getResources().getColor(R.color.clicked_Button_color));

        genderinp = genderTextView.getText().toString();
        if(genderinp.equals("Male")){
            genderTextView.setText("Female");

        } else if (genderinp.equals("Female"))
        {
            genderTextView.setText("Male");
        } else
        {
            genderTextView.setText("Male");
        }

        genderset = genderTextView.getText().toString();
        ParseUser currentUser = ParseUser.getCurrentUser();
        String user_id = currentUser.get("user_id").toString();
        Log.d("#### "+ Utils.getLogMsgId(), user_id);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("user_creds");
        query.whereEqualTo("user_id", user_id);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (object == null) {
                    Log.d("#### " + Utils.getLogMsgId(), "The getFirst request failed.");
                } else {
                    object.remove("gender");
                    object.put("gender", genderset);
                    user.setGender(genderset);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Log.d("#### " + Utils.getLogMsgId(), "Gender saved successfully");
                                userdata.putSerializable("user", user);
                                editProfileFragment.updateVals(userdata);
                            } else {
                                Log.e("#### " + Utils.getLogMsgId(), e.getMessage());
                            }
                        }
                    });
                }
            }
        });

        genderTextView.setBackgroundColor(getResources().getColor(R.color.pure_transparent_color));
        return;
    }

    public String getCurrentUsersName () {
        return user.getFull_name();
    }

    public void refreshChatFragment () {
        chatfragment.onResume();
    }

    public int is_user_blocked (String target_user) {
        final ArrayList<String> tmp_blocked_list = user.getBlocked_user_ids();

        if (tmp_blocked_list == null)
            return 0;

        if (tmp_blocked_list.contains(target_user))
            return 1;
        else
            return 0;
    }

    public void update_blocked_list (String target_user, String action) {
        ParseUser currentUser = ParseUser.getCurrentUser();
        String user_id = currentUser.get("user_id").toString();
        ArrayList<String> tmp_blocked_list = user.getBlocked_user_ids();

        if (tmp_blocked_list == null)
            tmp_blocked_list = new ArrayList<String>();

        if (action.equals("BLOCK"))
            tmp_blocked_list.add(target_user);
        else if (action.equals("UNBLOCK"))
            tmp_blocked_list.remove(target_user);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("user_creds");
        query.whereEqualTo("user_id", user_id);
        final ArrayList<String> finalTmp_blocked_list = tmp_blocked_list;
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (object == null) {
                    Log.d("#### " + Utils.getLogMsgId(), "The getFirst request failed.");
                } else {
                    object.remove("blocked_list");
                    object.put("blocked_list", finalTmp_blocked_list);
                    user.setBlocked_user_ids(finalTmp_blocked_list);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Log.d("#### " + Utils.getLogMsgId(), "Blocked List saved successfully");
                                userdata.putSerializable("user", user);
                            } else {
                                Log.e("#### " + Utils.getLogMsgId(), e.getMessage());
                            }
                        }
                    });
                }
            }
        });
    }

    public void showDialogMsg(String log_level, String err_string) {
        final AlertDialog alertDialog = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_DARK).create();

        alertDialog.setTitle(log_level);
        alertDialog.setMessage(err_string);
        alertDialog.setIcon(android.R.drawable.ic_dialog_alert);

        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    public void hideSplash() {
        TextView boei_logo = (TextView) findViewById(R.id.boei_logo);
        ProgressBar splash_progress_bar = (ProgressBar) findViewById(R.id.splash_progressBar);
        ImageView boei_logo_imageView = (ImageView) findViewById(R.id.boei_logo_imageview);
        boei_logo.setVisibility(View.INVISIBLE);
        splash_progress_bar.setVisibility(View.INVISIBLE);
        boei_logo_imageView.setVisibility(View.INVISIBLE);
    }

    public void startTheShow () {

        synchronized (starting_show_mutex) {
            if (getting_user_stat_complete == false) {
                Log.d("#### "+Utils.getLogMsgId(), "Waiting for user data...");
            } else if (getting_isbn_data_complete == false) {
                Log.d("#### " + Utils.getLogMsgId(), "Waiting for isbn data...");
            } else if (getting_sendbird_conn == false) {
                Log.d("#### " + Utils.getLogMsgId(), "Waiting for sendbird registration...");
            } else if (show_started == true) {
                Log.d("#### "+Utils.getLogMsgId(), "Show has already started");
            } else {
                userdata.putSerializable("user", user);
                chatfragment.setArguments(userdata);
                myProfileFragment.setArguments(userdata);
                editProfileFragment.setArguments(userdata);
                mobileVerificationFragment.setArguments(userdata);
                myBooksFragment.setArguments(userdata);
                walletFragment.setArguments(userdata);
                updateSendbirdUserInfo();
                Log.d("#### " + Utils.getLogMsgId(), "Bundle set with data");
                hideSplash();
                viewPager.setAdapter(new MyAdapter(getSupportFragmentManager()));
                PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.slidingtabs);
                tabs.setViewPager(viewPager);
                show_started = true;
            }
        }
    }

    public void fetchIsbnData ()
    {
        Log.d("#### "+ Utils.getLogMsgId(), "Inside fetchIsbnData");
        users_books = new ArrayList<books>();

        List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();

        if (user.getBook_isbns() == null) {
            synchronized (starting_show_mutex) {
                getting_isbn_data_complete = true;
            }
            startTheShow();
            return;
        }

        for (int i =0; i<user.getBook_isbns().size(); i++) {
            StringTokenizer st = new StringTokenizer(user.getBook_isbns().get(i));
            String isbn_num = st.nextToken("-");
            ParseQuery<ParseObject> query = ParseQuery.getQuery("books");
            query.whereEqualTo("isbn", isbn_num);
            queries.add(query);
            Log.d("#### "+ Utils.getLogMsgId(), user.getBook_isbns().get(i));
        }

        ParseQuery<ParseObject> mainQuery = ParseQuery.or(queries);

        mainQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    for (int j = 0; j < objects.size(); j++) {
                        books Book = new books();
                        Book.setBook_isbn((String) objects.get(j).get("isbn"));
                        Book.setBook_name((String) objects.get(j).get("title"));
                        Book.setAuthor_name((String) objects.get(j).get("author"));
                        Book.setImageLinks((String) objects.get(j).get("image_url"));
                        Book.setUsers((List<String>) objects.get(j).get("users"));
                        users_books.add(Book);
                        Log.d("#### "+ Utils.getLogMsgId(), (String) objects.get(j).get("image_url"));
                    }
                    user.setUserbooks((ArrayList<books>) users_books);
                } else {
                    Log.e("#### "+ Utils.getLogMsgId(), "Error in fetching ISBN data: " + e.getMessage());
                }

                synchronized (starting_show_mutex) {
                    getting_isbn_data_complete = true;
                }
                startTheShow();
            }
        });
    }

    public void updateUserBooks (String isbn, String title, String author, String image_url,
                                 ArrayList<String> users) {
        books Book = new books();
        Book.setBook_isbn(isbn);
        Book.setBook_name(title);
        Book.setAuthor_name(author);
        Book.setImageLinks(image_url);
        Book.setUsers(users);
        users_books.add(Book);
        user.setUserbooks((ArrayList<books>) users_books);
        userdata.putSerializable("user", user);
        Log.d("#### "+ Utils.getLogMsgId(), "Updated User Books");
    }

    public void saveInBooksTable (final String title, final String author, final String isbn13,
                                  final String user_id, final String imageLinks)
    {
        Log.d("#### "+ Utils.getLogMsgId(), "Inside saveInBooksTable");

        final String key = title+author;

        ParseQuery<ParseObject> query = ParseQuery.getQuery("books");
        query.whereEqualTo("key", key);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (object == null) {
                    Log.d("#### "+ Utils.getLogMsgId(), "The getFirst request failed.");
                    final ArrayList<String> users = new ArrayList<String>();

                    ParseObject books = new ParseObject("books");
                    books.put("key", key);
                    books.put("title", title);
                    books.put("author", author);
                    users.add(user_id);
                    books.put("users", users);
                    books.put("isbn", isbn13);
                    books.put("image_url", imageLinks);
                    updateUserBooks(isbn13, title, author, imageLinks, users);
                    myBooksFragment.updateBookShelf(userdata);

                    books.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Log.d("#### "+ Utils.getLogMsgId(), "Book added to books table");
                            } else {
                                Log.e("#### "+ Utils.getLogMsgId(), e.getMessage());
                            }
                        }
                    });

                } else {
                    ArrayList<String> users = (ArrayList<String>) object.get("users");
                    object.remove("users");
                    if (users == null) {
                        users = new ArrayList<String>();
                    }

                    users.add(user_id);
                    updateUserBooks((String) object.get("isbn"), (String) object.get("title"),
                            (String) object.get("author"), (String) object.get("image_url"), users);
                    myBooksFragment.updateBookShelf(userdata);

                    object.put("users", users);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Log.d("#### "+ Utils.getLogMsgId(), "Book updated at books table");
                            } else {
                                Log.e("#### "+ Utils.getLogMsgId(), "Error in saving book to book table: " + e.getMessage());
                            }
                        }
                    });
                }
            }
        });
    }

    public void saveBookIsbn(final String isbn13, final String author, final String title, final String imageLinks,
                             final String rentprice, final String resaleprice) {
        Log.d("#### "+ Utils.getLogMsgId(), "Inside saveBookIsbn");

        ParseUser currentUser = ParseUser.getCurrentUser();
        String user_id = currentUser.get("user_id").toString();
        ArrayList<String> bookIsbndetails = user.getBook_isbns();

        int found_match = 0;

        if (bookIsbndetails == null) {
            bookIsbndetails = new ArrayList<String>();
        } else{
            // Go through all the book details of the user and fetch
            // isbn and rent price and resale price. If the user is trying
            // to add the same book with the same details, then fail.
            // else let him add.
            for (String details: bookIsbndetails) {
                StringTokenizer st = new StringTokenizer(details);
                String isbn = st.nextToken("-");
                String rent_price = st.nextToken("-");
                String resale_price = st.nextToken("-");
                if (isbn.equals(isbn13)) {
                    found_match = 1;
                    if (rent_price.equals(rentprice) && resale_price.equals(resaleprice)) {
                        Log.d("#### "+ Utils.getLogMsgId(), "This book is already added to your bookshelf.");
                        showDialogMsg("Error", "This book is already present in your bookshelf");
                        addBookFragment.searchProgressBar.setVisibility(View.INVISIBLE);
                        return;
                    } else {
                        // This will remove the current entry in the users table and add
                        // new one with new details.
                        bookIsbndetails.remove(isbn+"-"+rent_price+"-"+resale_price);
                    }
                }
            }
        }

        if (found_match == 0)
            saveInBooksTable(title, author, isbn13, user_id, imageLinks);

        bookIsbndetails.add(isbn13+"-"+rentprice+"-"+resaleprice);
        user.setBook_isbns(bookIsbndetails);
        userdata.putSerializable("user", user);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("user_creds");
        query.whereEqualTo("user_id", user_id);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (object == null) {
                    Log.d("#### "+ Utils.getLogMsgId(), "The getFirst request failed.");
                } else {
                    object.remove("book_isbns");
                    object.put("book_isbns", user.getBook_isbns());
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Log.d("#### "+ Utils.getLogMsgId(), "book_isbns saved successfully");
                                Toast.makeText(getApplicationContext(), "Successfully Added To Your Bookshelf", Toast.LENGTH_LONG).show();
                                addBookFragment.searchProgressBar.setVisibility(View.INVISIBLE);


                                myBooksFragment.updateBookShelf(userdata);




                            } else {
                                Log.e("#### "+ Utils.getLogMsgId(), "Error in saving book isbn: "+e.getMessage());
                            }
                        }
                    });
                }
            }
        });

        return;
    }

    public void saveaddress(double latitude, double longitude, final String address)
    {
        Log.d("#### "+ Utils.getLogMsgId(), "inside save address");

        final ProgressBar imageUploadProgressBar = (ProgressBar) findViewById(R.id.imgUploadProgressBar);
        imageUploadProgressBar.setVisibility(View.VISIBLE);

        ParseUser currentUser = ParseUser.getCurrentUser();
        String user_id = currentUser.get("user_id").toString();

       parseGeoPoint = new ParseGeoPoint(latitude,longitude);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("user_creds");
        query.whereEqualTo("user_id", user_id);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (object == null) {
                    Log.d("#### "+ Utils.getLogMsgId(), "The getFirst request failed.");
                } else {
                    object.remove("location");
                    object.put("location", parseGeoPoint);
                    object.remove("address");
                    object.put("address", address);
                    user.setAddress(address);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Log.d("#### "+ Utils.getLogMsgId(), "Address saved successfully");
                                userdata.putSerializable("user", user);
                                editProfileFragment.updateVals(userdata);
                            } else {
                                Log.e("#### "+ Utils.getLogMsgId(), e.getMessage());
                            }
                            imageUploadProgressBar.setVisibility(View.INVISIBLE);
                        }
                    });
                }
            }
        });

        return;
    }




    public void saveMobileNumber (final String mobile_number) {
        Log.d("#### "+ Utils.getLogMsgId(), "Inside Save Mobile Number");

        final ProgressBar imageUploadProgressBar = (ProgressBar) findViewById(R.id.imgUploadProgressBar);
        imageUploadProgressBar.setVisibility(View.VISIBLE);

        ParseUser currentUser = ParseUser.getCurrentUser();
        String user_id = currentUser.get("user_id").toString();
        Log.d("#### "+ Utils.getLogMsgId(), mobile_number);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("user_creds");
        query.whereEqualTo("user_id", user_id);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (object == null) {
                    Log.d("#### "+ Utils.getLogMsgId(), "The getFirst request failed.");
                } else {
                    object.remove("mobile_number");
                    object.put("mobile_number", mobile_number);
                    user.setMobile_number(mobile_number);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Log.d("#### "+ Utils.getLogMsgId(), "Mobile Number saved successfully");
                                userdata.putSerializable("user", user);
                                editProfileFragment.updateVals(userdata);
                            } else {
                                Log.e("#### "+ Utils.getLogMsgId(), e.getMessage());
                            }
                            imageUploadProgressBar.setVisibility(View.INVISIBLE);
                        }
                    });
                }
            }
        });
        return;
    }

    public void saveDOB (final Calendar dob, final int age) {
        TextView ageTextView = (TextView) findViewById(R.id.dob_button);
        final Date dobDate = dob.getTime();
        final SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");

        if (age < 0) {
            ageTextView.setBackgroundColor(getResources().getColor(R.color.pure_transparent_color));
            return;
        }

        editProfileFragment.setImageUploadProgressBar(true);

        ParseUser currentUser = ParseUser.getCurrentUser();
        String user_id = currentUser.get("user_id").toString();
        Log.d("#### "+ Utils.getLogMsgId(), user_id);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("user_creds");
        query.whereEqualTo("user_id", user_id);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (object == null) {
                    Log.d("#### "+ Utils.getLogMsgId(), "The getFirst request failed.");
                } else {
                    object.remove("DOB");
                    object.put("DOB", dobDate);
                    user.setDOB(((Date) dobDate));
                    final String date = format.format(dobDate);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Log.d("#### "+ Utils.getLogMsgId(), "DOB saved successfully");
                                userdata.putSerializable("user", user);
                                editProfileFragment.updateVals(userdata);
                            } else {
                                Log.e("#### "+ Utils.getLogMsgId(), e.getMessage());
                            }
                            editProfileFragment.setImageUploadProgressBar(false);
                        }
                    });
                }
            }
        });
        ageTextView.setBackgroundColor(getResources().getColor(R.color.pure_transparent_color));
        return;
    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        }
        else {
            getFragmentManager().popBackStack();
        }
    }

    public void goToMyBookShelf (View v) {
        Log.d ("#### "+ Utils.getLogMsgId(), "Inside goToMyBookShelf");
        this.getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.slide_in_left, R.animator.slide_out_left,
                        R.animator.slide_out_right, R.animator.slide_in_right)
                .replace(R.id.my_profile_fragment, myBooksFragment, null)
                .addToBackStack(null)
                .commit();
    }


    public void goToEditProfile (View v) {
        Log.d("#### "+ Utils.getLogMsgId(), "Inside goToEditProfile");
        this.getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.slide_in_left, R.animator.slide_out_left,
                        R.animator.slide_out_right, R.animator.slide_in_right)
                .replace(R.id.my_profile_fragment, editProfileFragment, null)
                .addToBackStack(null)
                .commit();
    }

    public void goToAddBooks (View v) {
        Log.d("#### "+ Utils.getLogMsgId(), "Inside goToAddBooks");
        this.getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.fade_in, R.animator.fade_out)
                .replace(R.id.my_books_fragment, addBookFragment, null)
                .addToBackStack(null)
                .commit();
    }



    public void goToViewUser (Bundle userdata) {
        view_User_Fragment.setArguments(userdata);
        Log.d("#### "+ Utils.getLogMsgId(), "Inside goToViewUser");
        this.getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.slide_in_left, R.animator.slide_out_left,
                        R.animator.slide_out_right, R.animator.slide_in_right)
                .replace(R.id.myViewFlipper, view_User_Fragment, null)
                .addToBackStack(null)
                .commit();
    }

    public void goToWallet (View v) {
        Log.d("#### "+ Utils.getLogMsgId(), "Inside goToWallet");
        this.getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.slide_in_left, R.animator.slide_out_left,
                        R.animator.slide_out_right, R.animator.slide_in_right)
                .replace(R.id.my_profile_fragment, walletFragment, null)
                .addToBackStack(null)
                .commit();
    }

    public void showPicOpsPopup (View v) {
        final Dialog dialog = new Dialog(this);
        LayoutInflater li =  (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View picOps = li.inflate(R.layout.picopsdialog, null, false);

        picOpsListView = (ListView) picOps.findViewById(R.id.picops);
        picOpsList = new ArrayList<String>();
        if (isPicPresent == true) {
            picOpsList.add("Change Photo");
            picOpsList.add("Remove Photo");
        } else {
            picOpsList.add("Upload Photo");
        }

        picOpsListView.setAdapter(new picOpsAdapter(this));
        picOpsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    onClickPickPhoto(view);
                    dialog.dismiss();
                } else if (position == 1) {
                    onClickRemovePhoto(view);
                    dialog.dismiss();
                }
                Log.d("#### "+ Utils.getLogMsgId(), "Clicked position " + position);
            }
        });


        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(picOps);
        dialog.show();
    }

    private class picOpsAdapter extends BaseAdapter {

        private Context context;

        public picOpsAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return picOpsList.size();
        }

        @Override
        public Object getItem(int position) {
            return picOpsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View picOpsRowView = inflater.inflate(R.layout.picopsrows, null);

            TextView textViewRow = (TextView) picOpsRowView.findViewById(R.id.picOpsRow);
            textViewRow.setText(picOpsList.get(position));
            return picOpsRowView;
        }
    }
}
