package com.itz_mine.mark_i;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;
import com.sendbird.android.shadow.okhttp3.internal.Util;

import java.util.List;


public class SignUpActivity extends ActionBarActivity {
    private static TextView display_msg = null;
    Intent welcomeIntent = null;
    private EditText full_name_edit_text = null;
    private EditText email_id_edit_text = null;
    private EditText password_edit_text = null;
    private EditText confirm_password_edit_Text = null;
    TextView boei_logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        boei_logo = (TextView) findViewById(R.id.bookesh_logo);
        setBookeshLogo();
        setEmailnPasswordEditTexts();
        final Context this_ctxt = this;
        welcomeIntent = new Intent(this, WelcomeActivity.class);
    }

    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

    public void setBookeshLogo () {
        Typeface blackJack_tf = Typeface.createFromAsset(getAssets(), "fonts/black_jack.ttf");
        boei_logo.setTypeface(blackJack_tf);
    }

    public void setEmailnPasswordEditTexts () {
        full_name_edit_text = (EditText) findViewById(R.id.full_name_editText);
        email_id_edit_text = (EditText) findViewById(R.id.email_id_edittext);
        password_edit_text = (EditText) findViewById(R.id.password_edittext);
        confirm_password_edit_Text = (EditText) findViewById(R.id.confirm_password_editText);

        full_name_edit_text.addTextChangedListener(watcher);
        email_id_edit_text.addTextChangedListener(watcher);
        password_edit_text.addTextChangedListener(watcher);
        confirm_password_edit_Text.addTextChangedListener(watcher);
        Button signUpButton = (Button) findViewById(R.id.sign_up_button);
        signUpButton.setTextColor(Color.GRAY);
        signUpButton.setEnabled(false);
    }

    private final TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            Button signUpButton = (Button) findViewById(R.id.sign_up_button);
            if ((full_name_edit_text.getText().length() == 0) || (email_id_edit_text.getText().length() == 0) ||
                    (password_edit_text.getText().length() == 0) || (confirm_password_edit_Text.getText().length() == 0)) {
                signUpButton.setEnabled(false);
                signUpButton.setTextColor(Color.GRAY);
            }else {
                signUpButton.setEnabled(true);
                signUpButton.setTextColor(getResources().getColor(R.color.bookesh_logo_color));
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public void onSignUpClick (View v) {
        String full_name = full_name_edit_text.getText().toString();
        String email_id = email_id_edit_text.getText().toString();
        String password = password_edit_text.getText().toString();
        String confirm_password = confirm_password_edit_Text.getText().toString();


        Button signUpButton = (Button) findViewById(R.id.sign_up_button);
        signUpButton.setBackgroundColor(getResources().getColor(R.color.clicked_Button_color));

        if (!password.equals(confirm_password)) {
            // Show the failure view, clear the password and confirm password field and wait for the user to enter password again.
            displaymessage("Passwords do not match");

            password_edit_text.setText("");
            confirm_password_edit_Text.setText("");
            signUpButton.setBackgroundColor(getResources().getColor(R.color.transparent_Button_color));
            return;
        }
        signUpNewUser(full_name, email_id, password);
    }

    public void create_user_cred (final ParseUser user, final String user_id, final String full_name) {
        /* Fetch user info from user_creds db */
        final boolean[] is_match = {false};
        ParseQuery query = new ParseQuery("user_creds");
        Log.d("#### "+Utils.getLogMsgId(), user_id);
        query.whereEqualTo("user_id", user_id);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                Log.d("#### "+Utils.getLogMsgId(), "Done");
                if (e == null) {
                    Log.d("#### "+Utils.getLogMsgId(), "TEST");
                    for (ParseObject object : list) {
                        is_match[0] = true;
                    }

                    if (is_match[0] == false) {
                        /* Saving user data in the pseudo user class user_creds so as to manage one account for multiple logins */
                        ParseObject user_creds = new ParseObject("user_creds");
                        user_creds.put("user_id", user_id);
                        user_creds.put("full_name", full_name);
                        user_creds.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    user.saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            if (e == null) {
                                                startWelcomeActivity();
                                            } else {
                                                Log.d("#### "+Utils.getLogMsgId(), e.getMessage());
                                            }
                                        }
                                    });
                                } else {
                                    Log.d("#### "+Utils.getLogMsgId(), e.getMessage());
                                }
                            }
                        });
                    } else {
                        user.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    startWelcomeActivity();
                                } else {
                                    Log.d("#### "+Utils.getLogMsgId(), e.getMessage());
                                }
                            }
                        });
                    }
                } else {
                    Log.d("#### "+Utils.getLogMsgId(), e.getMessage());
                }
            }
        });
    }

    public void startWelcomeActivity () {
        Intent welcomeIntent = new Intent(this, WelcomeActivity.class);
        startActivity(welcomeIntent);
        finish();
    }

    public void signUpNewUser (String full_name, final String email_id, String password) {
        // Save new user data into Parse.com Data Storage
        final Button signUpButton = (Button) findViewById(R.id.sign_up_button);
        ParseUser user = new ParseUser();
        user.setUsername(email_id);
        user.setPassword(password);
        user.put("user_id", email_id);

        /* Saving user data in the pseudo user class user_creds so as to manage one account for multiple logins */
        create_user_cred(user, email_id,full_name);
        user.signUpInBackground(new SignUpCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    // Show a simple Toast message upon successful registration
                    displaymessage("Successfully Signed Up!");
                    startWelcomeActivity();
                    finish();
                } else {
                    displaymessage(e.getMessage());
                    signUpButton.setBackgroundColor(getResources().getColor(R.color.transparent_Button_color));
                }
            }
        });
    }

    public void displaymessage (String msg) {
        Resources res = getResources();
        LinearLayout linear_layout_buttons = (LinearLayout) findViewById(R.id.Linear_Layout_Buttons);
        if (display_msg == null) {
            display_msg = new TextView(this);
            linear_layout_buttons.addView(display_msg);
        }

        display_msg.setText(msg);
        display_msg.setTextSize(15);
        display_msg.setTextColor(res.getColor(R.color.editText_highlight_color));
    }
}
