package com.itz_mine.mark_i;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        TextView ageTextView = (TextView)getActivity().findViewById(R.id.dob_button);
        ageTextView.setBackgroundColor(getResources().getColor(R.color.pure_transparent_color));
        super.onCancel(dialog);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        if (!view.isShown())
            return;

        int age;
        Calendar dob = Calendar.getInstance();
        dob.set (year, month, day);

        age = ((WelcomeActivity)getActivity()).getAge(dob);

        Log.d("#### "+Utils.getLogMsgId(), "age = " + age);

        ((WelcomeActivity)getActivity()).saveDOB(dob,age);
    }
}
