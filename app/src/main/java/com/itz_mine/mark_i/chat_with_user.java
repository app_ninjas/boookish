package com.itz_mine.mark_i;


import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseUser;
import com.pkmmte.view.CircularImageView;
import com.sendbird.android.AdminMessage;
import com.sendbird.android.BaseChannel;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.FileMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.PreviousMessageListQuery;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.sendbird.android.UserMessage;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class chat_with_user extends Fragment {

    Bundle userdata;
    user_creds other_user;
    SendBirdMessagingAdapter mAdapter;
    ListView mListView;
    PreviousMessageListQuery mPrevMessageListQuery;
    Button mBtnSend;
    EditText mEtxtMessage;
    GroupChannel mGroupChannel;
    boolean mIsUploading;
    static final String identifier = "SendBirdGroupChat";
    String mChannelUrl;
    CircularImageView other_users_pic;
    TextView other_users_name;
    User other_sendbird_user;
    ImageView block_unblock_img;
    String other_users_block_status;
    String msg;

    public chat_with_user() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView =  inflater.inflate(R.layout.fragment_chat_with_user, container, false);
        userdata = null;
        mGroupChannel = null;
        mChannelUrl = null;
        mIsUploading = false;
        userdata = this.getArguments();
        other_users_pic = (CircularImageView) fragmentView.findViewById(R.id.user_pic);
        other_users_name = (TextView) fragmentView.findViewById(R.id.chat_user_name);
        other_users_pic.addShadow();
        block_unblock_img = (ImageView) fragmentView.findViewById(R.id.block_unblock_image);

        if (userdata == null) {
            Log.e("#### "+Utils.getLogMsgId(), "EMPTY BUNDLE RECEIVED");
        } else
            Log.d("#### "+Utils.getLogMsgId(), "BUNDLE IS NOT EMPTY");

        other_user = (user_creds) userdata.getSerializable("user");
        if (other_user == null) {
            Log.e ("#### "+Utils.getLogMsgId(), "GOT NULL user_creds FROM BUNDLE");
        } else
            Log.d("#### "+Utils.getLogMsgId(), "Got data from bundle Other User");

        msg = other_user.getMsg();
        if (msg == null) {
            Log.d("#### "+Utils.getLogMsgId(), "No msg in userdata");
        }else
            Log.d("#### "+Utils.getLogMsgId(), "Msg = "+msg);

        mListView = (ListView) fragmentView.findViewById(R.id.list);
        turnOffListViewDecoration(mListView);
        mChannelUrl = other_user.getChannel_url();
        final String current_users_name = ((WelcomeActivity)getActivity()).getCurrentUsersName();

        GroupChannel.getChannel(mChannelUrl, new GroupChannel.GroupChannelGetHandler() {
            @Override
            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                if (e != null) {
                    Toast.makeText(getActivity(), "" + e.getCode() + ":" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("#### "+Utils.getLogMsgId(), "Could not find channel with url "+ mChannelUrl+" Error: "+e.getMessage());
                    return;
                } else
                    Log.d("#### "+Utils.getLogMsgId(), "Found channel with url "+ mChannelUrl);

                mGroupChannel = groupChannel;
                mGroupChannel.markAsRead();

                String profile_pic_url = null;
                String ou_name = null;

                if (current_users_name.matches(mGroupChannel.getMembers().get(0).getNickname())) {
                    other_sendbird_user = mGroupChannel.getMembers().get(1);
                } else if (current_users_name.matches(mGroupChannel.getMembers().get(1).getNickname())) {
                    other_sendbird_user = mGroupChannel.getMembers().get(0);
                } else {
                    Log.e("#### "+Utils.getLogMsgId(), "None of the names match with current user");
                }

                profile_pic_url = other_sendbird_user.getProfileUrl();
                ou_name = other_sendbird_user.getNickname();

                if (profile_pic_url != null) {
                    Log.d("#### "+Utils.getLogMsgId(), "Profile Pic Url: "+profile_pic_url);
                    Picasso.with(getActivity()).load(profile_pic_url).noPlaceholder().into(other_users_pic);
                }

                if (ou_name != null)
                    other_users_name.setText(ou_name);

                if (((WelcomeActivity)getActivity()).is_user_blocked(other_sendbird_user.getUserId()) == 0) {
                    other_users_block_status = "UNBLOCKED";
                    block_unblock_img.setImageResource(R.mipmap.ic_action_block_user);
                } else {
                    other_users_block_status = "BLOCKED";
                    block_unblock_img.setImageResource(R.mipmap.ic_action_unblock_user);
                }

                block_unblock_img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (other_users_block_status.equals("BLOCKED"))
                            handle_block_unblock ("UNBLOCK");
                        else if (other_users_block_status.equals("UNBLOCKED"))
                            handle_block_unblock("BLOCK");
                    }
                });

                mAdapter = new SendBirdMessagingAdapter(getActivity(), mGroupChannel);
                mListView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
                loadPrevMessages(true, groupChannel);
                if (msg != null) {
                    send(msg);
                    msg = null;
                }
            }
        });

        mBtnSend = (Button) fragmentView.findViewById(R.id.btn_send);
        mBtnSend.setEnabled(false);
        mBtnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                send(null);
            }
        });

        mEtxtMessage = (EditText) fragmentView.findViewById(R.id.etxt_message);
        mEtxtMessage.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        send(null);
                    }
                    return true; // Do not hide keyboard.
                }

                return false;
            }
        });

        mEtxtMessage.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        mEtxtMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mBtnSend.setEnabled(s.length() > 0);

                if (s.length() == 1) {
                    if (mGroupChannel != null)
                        mGroupChannel.startTyping();
                } else if (s.length() <= 0) {
                    if (mGroupChannel != null)
                        mGroupChannel.endTyping();
                }
            }
        });

        Log.d("#### "+Utils.getLogMsgId(), "Returning fragmentview");

        return fragmentView;
    }

    public void handle_block_unblock (final String action) {
        if (action.equals("BLOCK")) {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Block User")
                    .setMessage("Do you want to block " + other_sendbird_user.getNickname() + "?")
                    .setPositiveButton("Block", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SendBird.blockUser(other_sendbird_user, new SendBird.UserBlockHandler() {
                                @Override
                                public void onBlocked(User user, SendBirdException e) {
                                    if (e == null) {
                                        Log.d("#### "+Utils.getLogMsgId(), other_sendbird_user.getNickname() + " successfully blocked");
                                        ((WelcomeActivity)getActivity()).update_blocked_list(other_sendbird_user.getUserId(), action);
                                        other_users_block_status = "BLOCKED";
                                        block_unblock_img.setImageResource(R.mipmap.ic_action_unblock_user);
                                    } else
                                        Log.e("#### "+Utils.getLogMsgId(), "Failed to block "+other_sendbird_user.getNickname()+". Error: "+e.getMessage());
                                }
                            });
                        }
                    })
                    .setNegativeButton("Cancel", null).create().show();
        } else if (action.equals("UNBLOCK")) {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Unblock User")
                    .setMessage("Do you want to unblock " + other_sendbird_user.getNickname() + "?")
                    .setPositiveButton("Unblock", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SendBird.unblockUser(other_sendbird_user, new SendBird.UserUnblockHandler() {
                                @Override
                                public void onUnblocked(SendBirdException e) {
                                    if (e == null) {
                                        Log.d("#### "+Utils.getLogMsgId(), other_sendbird_user.getNickname() + " successfully unblocked");
                                        ((WelcomeActivity)getActivity()).update_blocked_list(other_sendbird_user.getUserId(), action);
                                        other_users_block_status = "UNBLOCKED";
                                        block_unblock_img.setImageResource(R.mipmap.ic_action_block_user);
                                    }else
                                        Log.e("#### "+Utils.getLogMsgId(), "Failed to unblock "+other_sendbird_user.getNickname()+". Error: "+e.getMessage());
                                }
                            });
                        }
                    })
                    .setNegativeButton("Cancel", null).create().show();
        }
    }

    private void turnOffListViewDecoration(ListView listView) {
        listView.setDivider(null);
        listView.setDividerHeight(0);
        listView.setHorizontalFadingEdgeEnabled(false);
        listView.setVerticalFadingEdgeEnabled(false);
        listView.setHorizontalScrollBarEnabled(false);
        listView.setVerticalScrollBarEnabled(true);
        listView.setSelector(new ColorDrawable(0x00ffffff));
        listView.setCacheColorHint(0x00000000); // For Gingerbread scrolling bug fix
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mIsUploading) {
            SendBird.addChannelHandler(identifier, new SendBird.ChannelHandler() {
                @Override
                public void onMessageReceived(BaseChannel baseChannel, BaseMessage baseMessage) {
                    if (baseChannel.getUrl().equals(mChannelUrl)) {
                        if (mAdapter != null) {
                            mGroupChannel.markAsRead();
                            mAdapter.appendMessage(baseMessage);
                            mAdapter.notifyDataSetChanged();
                            ((WelcomeActivity)getActivity()).refreshChatFragment();
                        }
                    }
                }

                @Override
                public void onReadReceiptUpdated(GroupChannel groupChannel) {
                    if (groupChannel.getUrl().equals(mChannelUrl)) {
                        mAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onTypingStatusUpdated(GroupChannel groupChannel) {
                    if (groupChannel.getUrl().equals(mChannelUrl)) {
                        mAdapter.notifyDataSetChanged();
                    }
                }
            });

        } else {
            mIsUploading = false;

            /**
             * Set this as true to restart auto-background detection,
             * when your Activity is shown again after the external Activity is finished.
             */
            //SendBird.setAutoBackgroundDetection(true);
        }

    }

    private void send(String msg) {
        if (mEtxtMessage.getText().length() <= 0 && msg == null) {
            return;
        }

        if (msg == null) {
            msg = mEtxtMessage.getText().toString();
            mEtxtMessage.setText("");
        }

        mGroupChannel.sendUserMessage(msg, new BaseChannel.SendUserMessageHandler() {
            @Override
            public void onSent(UserMessage userMessage, SendBirdException e) {
                if (e != null) {
                    Toast.makeText(getActivity(), "" + e.getCode() + ":" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }

                mAdapter.appendMessage(userMessage);
                mAdapter.notifyDataSetChanged();

                ((WelcomeActivity) getActivity()).refreshChatFragment();
            }
        });

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Helper.hideKeyboard(getActivity());
        }
    }

    private void loadPrevMessages(final boolean refresh, GroupChannel groupChannel) {

        if (refresh || mPrevMessageListQuery == null) {
            mPrevMessageListQuery = groupChannel.createPreviousMessageListQuery();
        }

        if (mPrevMessageListQuery.isLoading()) {
            return;
        }

        if (!mPrevMessageListQuery.hasMore()) {
            return;
        }

        mPrevMessageListQuery.load(30, true, new PreviousMessageListQuery.MessageListQueryResult() {
            @Override
            public void onResult(List<BaseMessage> list, SendBirdException e) {
                if (e != null) {
                    Toast.makeText(getActivity(), "" + e.getCode() + ":" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (refresh) {
                    mAdapter.clear();
                }

                for (BaseMessage message : list) {
                    mAdapter.insertMessage(message);
                }
                mAdapter.notifyDataSetChanged();
                mListView.setSelection(list.size());
            }
        });
    }

    public static class SendBirdMessagingAdapter extends BaseAdapter {
        private static final int TYPE_UNSUPPORTED = 0;
        private static final int TYPE_USER_MESSAGE = 1;
        private static final int TYPE_ADMIN_MESSAGE = 2;
        private static final int TYPE_FILE_MESSAGE = 3;
        private static final int TYPE_TYPING_INDICATOR = 4;

        private final Context mContext;
        private final LayoutInflater mInflater;
        private final ArrayList<Object> mItemList;
        private final GroupChannel mGroupChannel;

        public SendBirdMessagingAdapter(Context context, GroupChannel channel) {
            mContext = context;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mItemList = new ArrayList<>();
            mGroupChannel = channel;
        }

        @Override
        public int getCount() {
            return mItemList.size() + (mGroupChannel.isTyping() ? 1 : 0);
        }

        @Override
        public Object getItem(int position) {
            if (position >= mItemList.size()) {
                List<User> members = mGroupChannel.getTypingMembers();
                ArrayList<String> names = new ArrayList<>();
                for (User member : members) {
                    names.add(member.getNickname());
                }

                return names;
            }
            return mItemList.get(position);
        }

        public void delete(Object object) {
            mItemList.remove(object);
        }

        public void clear() {
            mItemList.clear();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public void insertMessage(BaseMessage message) {
            mItemList.add(0, message);
        }

        public void appendMessage(BaseMessage message) {
            mItemList.add(message);
        }

        @Override
        public int getItemViewType(int position) {
            if (position >= mItemList.size()) {
                return TYPE_TYPING_INDICATOR;
            }

            Object item = mItemList.get(position);
            if (item instanceof UserMessage) {
                return TYPE_USER_MESSAGE;
            } else if (item instanceof FileMessage) {
                return TYPE_FILE_MESSAGE;
            } else if (item instanceof AdminMessage) {
                return TYPE_ADMIN_MESSAGE;
            }

            return TYPE_UNSUPPORTED;
        }

        @Override
        public int getViewTypeCount() {
            return 5;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            final Object item = getItem(position);

            if (convertView == null || ((ViewHolder) convertView.getTag()).getViewType() != getItemViewType(position)) {
                viewHolder = new ViewHolder();
                viewHolder.setViewType(getItemViewType(position));

                switch (getItemViewType(position)) {
                    case TYPE_UNSUPPORTED:
                        convertView = new View(mInflater.getContext());
                        convertView.setTag(viewHolder);
                        break;
                    case TYPE_USER_MESSAGE: {
                        TextView tv;
                        ImageView iv;
                        View v;

                        convertView = mInflater.inflate(R.layout.sendbird_view_group_user_message, parent, false);

                        v = convertView.findViewById(R.id.left_container);
                        viewHolder.setView("left_container", v);
                        tv = (TextView) convertView.findViewById(R.id.txt_left);
                        viewHolder.setView("left_message", tv);
                        tv = (TextView) convertView.findViewById(R.id.txt_left_time);
                        viewHolder.setView("left_time", tv);

                        v = convertView.findViewById(R.id.right_container);
                        viewHolder.setView("right_container", v);
                        tv = (TextView) convertView.findViewById(R.id.txt_right);
                        viewHolder.setView("right_message", tv);
                        tv = (TextView) convertView.findViewById(R.id.txt_right_name);
                        viewHolder.setView("right_name", tv);
                        tv = (TextView) convertView.findViewById(R.id.txt_right_time);
                        viewHolder.setView("right_time", tv);
                        tv = (TextView) convertView.findViewById(R.id.txt_right_status);
                        viewHolder.setView("right_status", tv);

                        convertView.setTag(viewHolder);
                        break;
                    }
                    case TYPE_ADMIN_MESSAGE: {
                        convertView = mInflater.inflate(R.layout.sendbird_view_admin_message, parent, false);
                        viewHolder.setView("message", convertView.findViewById(R.id.txt_message));
                        convertView.setTag(viewHolder);
                        break;
                    }
                    case TYPE_FILE_MESSAGE: {
                        TextView tv;
                        ImageView iv;
                        View v;

                        convertView = mInflater.inflate(R.layout.sendbird_view_group_file_message, parent, false);

                        v = convertView.findViewById(R.id.left_container);
                        viewHolder.setView("left_container", v);
                        iv = (ImageView) convertView.findViewById(R.id.img_left);
                        viewHolder.setView("left_image", iv);
                        tv = (TextView) convertView.findViewById(R.id.txt_left_time);
                        viewHolder.setView("left_time", tv);

                        v = convertView.findViewById(R.id.right_container);
                        viewHolder.setView("right_container", v);
                        iv = (ImageView) convertView.findViewById(R.id.img_right);
                        viewHolder.setView("right_image", iv);
                        tv = (TextView) convertView.findViewById(R.id.txt_right_name);
                        viewHolder.setView("right_name", tv);
                        tv = (TextView) convertView.findViewById(R.id.txt_right_time);
                        viewHolder.setView("right_time", tv);
                        tv = (TextView) convertView.findViewById(R.id.txt_right_status);
                        viewHolder.setView("right_status", tv);

                        convertView.setTag(viewHolder);
                        break;
                    }
                    case TYPE_TYPING_INDICATOR: {
                        convertView = mInflater.inflate(R.layout.sendbird_view_group_typing_indicator, parent, false);
                        viewHolder.setView("message", convertView.findViewById(R.id.txt_message));
                        convertView.setTag(viewHolder);
                        break;
                    }
                }
            }

            viewHolder = (ViewHolder) convertView.getTag();
            switch (getItemViewType(position)) {
                case TYPE_UNSUPPORTED:
                    break;
                case TYPE_USER_MESSAGE:
                    UserMessage message = (UserMessage) item;
                    if (message.getSender().getUserId().equals(SendBird.getCurrentUser().getUserId())) {
                        viewHolder.getView("left_container", View.class).setVisibility(View.GONE);
                        viewHolder.getView("right_container", View.class).setVisibility(View.VISIBLE);

                        viewHolder.getView("right_name", TextView.class).setText(message.getSender().getNickname());
                        viewHolder.getView("right_message", TextView.class).setText(message.getMessage());
                        viewHolder.getView("right_time", TextView.class).setText(Helper.getDisplayDateTime(mContext, message.getCreatedAt()));

                        int unreadCount = mGroupChannel.getReadReceipt(message);
                        if (unreadCount > 1) {
                            viewHolder.getView("right_status", TextView.class).setText("Unread " + unreadCount);
                        } else if (unreadCount == 1) {
                            viewHolder.getView("right_status", TextView.class).setText("Unread");
                        } else {
                            viewHolder.getView("right_status", TextView.class).setText("");
                        }

                    } else {
                        viewHolder.getView("left_container", View.class).setVisibility(View.VISIBLE);
                        viewHolder.getView("right_container", View.class).setVisibility(View.GONE);

                        viewHolder.getView("left_message", TextView.class).setText(message.getMessage());
                        viewHolder.getView("left_time", TextView.class).setText(Helper.getDisplayDateTime(mContext, message.getCreatedAt()));
                    }
                    break;
                case TYPE_ADMIN_MESSAGE:
                    AdminMessage adminMessage = (AdminMessage) item;
                    viewHolder.getView("message", TextView.class).setText(Html.fromHtml(adminMessage.getMessage()));
                    break;
                case TYPE_FILE_MESSAGE:
                    final FileMessage fileLink = (FileMessage) item;

                    if (fileLink.getSender().getUserId().equals(SendBird.getCurrentUser().getUserId())) {
                        viewHolder.getView("left_container", View.class).setVisibility(View.GONE);
                        viewHolder.getView("right_container", View.class).setVisibility(View.VISIBLE);

                        viewHolder.getView("right_name", TextView.class).setText(fileLink.getSender().getNickname());
                        if (fileLink.getType().toLowerCase().startsWith("image")) {
                            Helper.displayUrlImage(viewHolder.getView("right_image", ImageView.class), fileLink.getUrl());
                        } else {
                            viewHolder.getView("right_image", ImageView.class).setImageResource(R.drawable.sendbird_icon_file);
                        }
                        viewHolder.getView("right_time", TextView.class).setText(Helper.getDisplayDateTime(mContext, fileLink.getCreatedAt()));

                        int unreadCount = mGroupChannel.getReadReceipt(fileLink);
                        if (unreadCount > 1) {
                            viewHolder.getView("right_status", TextView.class).setText("Unread " + unreadCount);
                        } else if (unreadCount == 1) {
                            viewHolder.getView("right_status", TextView.class).setText("Unread");
                        } else {
                            viewHolder.getView("right_status", TextView.class).setText("");
                        }

                        viewHolder.getView("right_container").setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                new AlertDialog.Builder(mContext)
                                        .setTitle("SendBird")
                                        .setMessage("Do you want to download this file?")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                try {
                                                    Helper.downloadUrl(fileLink.getUrl(), fileLink.getName(), mContext);
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        })
                                        .setNegativeButton("Cancel", null)
                                        .create()
                                        .show();
                            }
                        });
                    } else {
                        viewHolder.getView("left_container", View.class).setVisibility(View.VISIBLE);
                        viewHolder.getView("right_container", View.class).setVisibility(View.GONE);

                        if (fileLink.getType().toLowerCase().startsWith("image")) {
                            Helper.displayUrlImage(viewHolder.getView("left_image", ImageView.class), fileLink.getUrl());
                        } else {
                            viewHolder.getView("left_image", ImageView.class).setImageResource(R.drawable.sendbird_icon_file);
                        }
                        viewHolder.getView("left_time", TextView.class).setText(Helper.getDisplayDateTime(mContext, fileLink.getCreatedAt()));

                        viewHolder.getView("left_container").setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                new AlertDialog.Builder(mContext)
                                        .setTitle("SendBird")
                                        .setMessage("Do you want to download this file?")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                try {
                                                    Helper.downloadUrl(fileLink.getUrl(), fileLink.getName(), mContext);
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        })
                                        .setNegativeButton("Cancel", null)
                                        .create()
                                        .show();
                            }
                        });
                    }
                    break;

                case TYPE_TYPING_INDICATOR: {
                    int itemCount = ((List) item).size();
                    String typeMsg = ((List) item).get(0)
                            + ((itemCount > 1) ? " +" + (itemCount - 1) : "")
                            + ((itemCount > 1) ? " are " : " is ")
                            + "typing...";
                    viewHolder.getView("message", TextView.class).setText(typeMsg);
                    break;
                }
            }

            return convertView;
        }

        private class ViewHolder {
            private Hashtable<String, View> holder = new Hashtable<>();
            private int type;

            public int getViewType() {
                return this.type;
            }

            public void setViewType(int type) {
                this.type = type;
            }

            public void setView(String k, View v) {
                holder.put(k, v);
            }

            public View getView(String k) {
                return holder.get(k);
            }

            public <T> T getView(String k, Class<T> type) {
                return type.cast(getView(k));
            }
        }
    }

}
