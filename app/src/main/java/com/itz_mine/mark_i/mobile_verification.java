package com.itz_mine.mark_i;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.app.Fragment;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pkmmte.view.CircularImageView;

import org.w3c.dom.Text;


/**
 * A simple {@link Fragment} subclass.
 */
public class mobile_verification extends Fragment {
    user_creds user;
    Button test;
    EditText mobilenum;
    EditText countrycode;
    public static int REQUEST_CODE = 969;
    String mobile;

    public mobile_verification() {
        // Required empty public constructor
    }

    ImageView blurredImageView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        String phone_number = null;
        View fragmentView =  inflater.inflate(R.layout.fragment_mobile_verification, container, false);

        Log.e("#### "+Utils.getLogMsgId(), "inside mobileverifyactivity-2");
        test = (Button) fragmentView.findViewById(R.id.verify_mobile_button);
        mobilenum = (EditText) fragmentView.findViewById(R.id.phone_num);
        countrycode = (EditText) fragmentView.findViewById(R.id.country_code);
        TextView mobnum = (TextView) fragmentView.findViewById(R.id.add_phone_num);

        blurredImageView = (ImageView) fragmentView.findViewById(R.id.blurred_bg);

        Bitmap bg_image;

        Bundle userdata = this.getArguments();
        if (userdata == null) {
            Log.e("#### "+Utils.getLogMsgId(), "EMPTY BUNDLE RECEIVED");
        } else
            Log.d("#### "+Utils.getLogMsgId(), "BUNDLE IS NOT EMPTY");

        user = (user_creds) userdata.getSerializable("user");
        if (user == null) {
            Log.e ("#### "+Utils.getLogMsgId(), "GOT NULL user_creds FROM BUNDLE");
        } else
            Log.d ("#### "+Utils.getLogMsgId(), "Got data from bundle: "+user.getFull_name());

        //setProfImages(user.getProfile_pic());

        phone_number = user.getMobile_number();
        if (phone_number != null)
            mobnum.setText(phone_number);
        else
            mobnum.setText("Update Mobile Number");

        if (test == null)
            Log.d("#### "+Utils.getLogMsgId(), "didn't get test");
        countrycode.setText(VerifyMobile
                .getCountryCode(getActivity().getApplication().getApplicationContext()));
        test.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mobile = countrycode.getText().toString()
                        + mobilenum.getText().toString();
                Log.d("#### "+Utils.getLogMsgId(), "inside mobileverifyactivity-3");
                Intent in = new Intent(getActivity(), VerifyMobile.class);
                in.putExtra("app_id", "801ee2bc85784dd9b850f2e");
                in.putExtra("access_token",
                        "e5a375e2d0eb7bc32eaf798ecf3122a723cc2e3b");
                in.putExtra("mobile", mobile);
                if (mobile.length() == 0) {
                    countrycode.setError("Please enter mobile number");
                } else {
                    if (CheckNetworkConnection
                            .isConnectionAvailable(getActivity().getApplication().getApplicationContext())) {
                        Log.d("#### "+Utils.getLogMsgId(), "inside ");
                        startActivityForResult(in, VerifyMobile.REQUEST_CODE);
                    } else {
                        Toast.makeText(getActivity().getApplication().getApplicationContext(),
                                "no internet connection", Toast.LENGTH_SHORT)
                                .show();
                    }
                }
            }
        });

        return fragmentView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("#### "+Utils.getLogMsgId(), "HITTING VERIFY MOBILE ACTIVITY RESULT");

        Log.d("#### "+Utils.getLogMsgId(), "VM: Inside OnActivityResult");

        if (requestCode == REQUEST_CODE)
            Log.d("#### "+Utils.getLogMsgId(), "VM: GOT REQ VER MOB");

        if ((requestCode == REQUEST_CODE)) {
            int result = data.getIntExtra("result", 0);

            if (result == 104) {
                Log.d("#### "+Utils.getLogMsgId(), "VM: Calliing save Mobile Number");
                ((WelcomeActivity)getActivity()).saveMobileNumber(mobile);
            }
        }
    }
}
