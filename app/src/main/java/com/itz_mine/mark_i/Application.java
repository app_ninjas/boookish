package com.itz_mine.mark_i;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.sendbird.android.SendBird;

/**
 * Created by root on 8/19/15.
 */
public class Application extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
        //Parse.enableLocalDatastore(this);
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId("0UpqpHM2jLyB0IhwZDbnYCjzd1zCfiet5LrGrHA1")
                .clientKey("itl1ANi6sCUQwEw9jxJgYtPw68yhnJTRudgqBwpe")
                .server("https://parseapi.back4app.com")
                .build()
        );
        //Parse.initialize(this, "0UpqpHM2jLyB0IhwZDbnYCjzd1zCfiet5LrGrHA1", "itl1ANi6sCUQwEw9jxJgYtPw68yhnJTRudgqBwpe");
        ParseFacebookUtils.initialize(this);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        SendBird.init("C04EA901-8095-4CD8-853E-4DC1731D8ED2", this);
        Log.d("#### "+Utils.getLogMsgId(), "Application: Init Complete");
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
