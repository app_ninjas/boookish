package com.itz_mine.mark_i;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.app.Fragment;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class add_book_fragment extends Fragment {
    private static final String ACTION_FOR_INTENT_CALLBACK = "THIS_IS_A_UNIQUE_KEY_WE_USE_TO_COMMUNICATE";
    private static final String ACTION_FOR_GOOGLEAPI_URL = "GOOGLE_API_URL";

    private static final String key="39WsvPkcYihCdrbE8eQQgg";
    private static final String SEARCH_API="https://www.goodreads.com/search/index.xml?key="+key+"&q=";
    private static final String GET_ISBN_API="https://www.goodreads.com/book/show/";
    private static final String GET_BOOK_DETAILS_API="https://www.googleapis.com/books/v1/volumes?q=";
    private String QUERY_URL = GET_BOOK_DETAILS_API;
    private String isbn13_of_book;
    private static String SEARCH_URL;
    View fragmentView;
    ProgressBar searchProgressBar;
    TextView result_page_summary;
    View result_summarydivider;
    ListView searchResultsDisplay;
    List<String> searchResultsList;
    bookSearchResults results;
    TextView prevTextView;
    TextView nextTextView;
    bookResult toBeAdded;

    public add_book_fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //getContent();
        fragmentView =  inflater.inflate(R.layout.fragment_add_book_fragment, container, false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        final EditText addBookSearchEditText = (EditText)fragmentView.findViewById(R.id.add_book_search);
        result_page_summary = (TextView) fragmentView.findViewById(R.id.resut_summary);
        searchProgressBar = (ProgressBar)fragmentView.findViewById(R.id.search_progressbar);
        result_summarydivider = (View) fragmentView.findViewById(R.id.result_summary_divider);
        searchResultsDisplay = (ListView) fragmentView.findViewById(R.id.search_results);
        prevTextView = (TextView) fragmentView.findViewById(R.id.prev_textview);
        nextTextView = (TextView) fragmentView.findViewById(R.id.next_textview);

        searchProgressBar.setVisibility(View.INVISIBLE);
        result_page_summary.setVisibility(View.INVISIBLE);
        result_summarydivider.setVisibility(View.INVISIBLE);
        prevTextView.setVisibility(View.INVISIBLE);
        nextTextView.setVisibility(View.INVISIBLE);

        prevTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPrevClick();
            }
        });

        nextTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNextClick();
            }
        });

        prevTextView.setEnabled(false);
        nextTextView.setEnabled(false);

        addBookSearchEditText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    onSearchBook(addBookSearchEditText);
                    return true;
                }
                return false;
            }
        });

        return fragmentView;
    }

    public void onNextClick () {
        int url_length = SEARCH_URL.length();

        String next_url = SEARCH_URL.substring(0, url_length -1) + new Character((char) (SEARCH_URL.charAt(url_length - 1) + 1));
        SEARCH_URL = next_url;
        getContent(next_url, ACTION_FOR_INTENT_CALLBACK);
    }

    public void onPrevClick () {
        int url_length = SEARCH_URL.length();

        String next_url = SEARCH_URL.substring(0, url_length -1) + new Character((char) (SEARCH_URL.charAt(url_length - 1) - 1));
        SEARCH_URL = next_url;
        getContent(next_url, ACTION_FOR_INTENT_CALLBACK);
    }

    public void onSearchBook (EditText addBookSearchEditText)
    {
        // Hide The Keyboard
        InputMethodManager inputManager =
                (InputMethodManager) getActivity().
                        getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(
                getActivity().getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);

        String addBookSearchQuery = addBookSearchEditText.getText().toString();

        if ((addBookSearchQuery != null) &&
                (addBookSearchQuery.trim().length() > 0)) {
            Log.d("#### "+Utils.getLogMsgId(), "Searched for " + addBookSearchQuery);
            SEARCH_URL = SEARCH_API+ addBookSearchQuery.replace(" ", "+") + "&page=1";
            getContent(SEARCH_URL, ACTION_FOR_INTENT_CALLBACK);
        }else
            Log.d("#### "+Utils.getLogMsgId(), "No Search String Given");
    }

    private void getContent(String search_url_with_query, String identifier)
    {
        // the request
        searchProgressBar.setVisibility(View.VISIBLE);
        try
        {
            Log.d("#### "+Utils.getLogMsgId(), "search_url_with_query = "+search_url_with_query);
            HttpGet httpGet = new HttpGet(new URI(search_url_with_query));
            RestTask task = new RestTask(getActivity(), identifier);
            task.execute(httpGet);
        }
        catch (Exception e)
        {
            Log.e("#### "+Utils.getLogMsgId(), e.getMessage());
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        getActivity().registerReceiver(receiver, new IntentFilter(ACTION_FOR_INTENT_CALLBACK));
        getActivity().registerReceiver(json_google_api_receiver, new IntentFilter(ACTION_FOR_GOOGLEAPI_URL));

    }

    @Override
    public void onPause()
    {
        super.onPause();
        getActivity().unregisterReceiver(receiver);
        getActivity().unregisterReceiver(json_google_api_receiver);

    }

    /**
     * Our Broadcast Receiver. We get notified that the data is ready this way.
     */
    private BroadcastReceiver receiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String response = intent.getStringExtra(RestTask.HTTP_RESPONSE);
            //Log.d("#### "+Utils.getLogMsgId(), "RESPONSE = " + response);
            parse_response(response);
        }
    };

    /**
     * Our Broadcast Receiver. We get notified that the data is ready this way.
     */
    private BroadcastReceiver json_google_api_receiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String response = intent.getStringExtra(RestTask.HTTP_RESPONSE);
            Log.d("#### "+Utils.getLogMsgId(), "RESPONSE = " + response);
            try {
                JSONObject reader = new JSONObject(response);
                Log.d ("#### "+Utils.getLogMsgId(), "Parse JSON GOOGLE API");
                parse_json_google_api_response("url_response", reader);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void parse_json_google_api_response (String identifier, JSONObject reader){
        String isbn13 = null;
        String title = null;
        String author = null;
        String imagelinks = null;
        int count = 0;
        for(int i = 0; i< reader.names().length(); i++){
            try {
                String key = reader.names().getString(i);
                if (key.equals("items")) {
                    JSONArray items = reader.getJSONArray(key);
                    for (int j = 0; j < items.length(); j++) {
                        parse_json_google_api_response("items", items.getJSONObject(j));
                    }
                }

                if (key.equals("volumeInfo")) {
                    parse_json_google_api_response("volumeInfo", reader.getJSONObject(key));
                }

                if (key.equals("title")) {
                    title = (String) reader.get(reader.names().getString(i));
                    count++;
                }

                if (key.equals("authors")) {
                    JSONArray items = reader.getJSONArray(key);
                    for (int j = 0; j < items.length(); j++) {
                        if (j == 0)
                            author = items.getString(j);
                        else
                            author = author + " & "+items.getString(j);
                    }
                    count++;
                }

                if (key.equals("imageLinks")) {
                    JSONObject imageObject  = reader.getJSONObject(key);
                    for (int j = 0; j < imageObject.names().length(); j++) {
                        String image_key = imageObject.names().getString(j);
                        if (image_key.equals("thumbnail")) {
                            imagelinks = (String) imageObject.get(image_key);
                        }
                    }
                    count++;
                }

                if (key.equals("industryIdentifiers")) {
                    JSONArray items = reader.getJSONArray(key);
                    for (int j = 0; j < items.length(); j++) {
                        JSONObject isbnObject = items.getJSONObject(j);
                        if (isbnObject.get("type").equals("ISBN_13")) {
                            isbn13 = (String) isbnObject.get("identifier");
                        }
                    }
                    count++;
                }

                if (count == 4) {
                    Log.d("#### "+Utils.getLogMsgId(), "Title : " + title);
                    if (isbn13_of_book.equals(isbn13)) {
                        Log.d("#### "+Utils.getLogMsgId(), "Title : " + title);
                        Log.d("#### "+Utils.getLogMsgId(), "Author : "+author);
                        Log.d("#### "+Utils.getLogMsgId(), "ISBN13 = "+isbn13);
                        Log.d("#### "+Utils.getLogMsgId(), "image_url = "+imagelinks);
                        Log.d("#### "+Utils.getLogMsgId(), "@@@@@@@@@@");
                       // ((WelcomeActivity)getActivity()).saveBookIsbn(isbn13, author, title, imagelinks);
                    }
                    count = 0;
                }

                //Log.d("#### "++Utils.getLogMsgId(), "key = " + reader.names().getString(i) + " value = " + reader.get(reader.names().getString(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public void display_results (bookSearchResults searchResults) {
        results = searchResults;
        bookResult book;
        int total_results = searchResults.getResult_end() - searchResults.getResult_start() + 1;
        int index = 0;

        if (searchResults.getResult_start() != 1){
            prevTextView.setVisibility(View.VISIBLE);
            prevTextView.setEnabled(true);
        } else {
            prevTextView.setVisibility(View.INVISIBLE);
            prevTextView.setEnabled(false);
        }

        if (searchResults.getResult_end() != searchResults.getTotal_results()) {
            nextTextView.setVisibility(View.VISIBLE);
            nextTextView.setEnabled(true);
        } else {
            nextTextView.setVisibility(View.INVISIBLE);
            nextTextView.setEnabled(false);
        }

        searchProgressBar.setVisibility(View.INVISIBLE);
        Log.d("#### "+Utils.getLogMsgId(), "$$Total Results : " + total_results);
        String summary = "Showing "+searchResults.getResult_start()+"-"+searchResults.getResult_end()+" of "+searchResults.getTotal_results()+" results";
        Log.d("#### "+Utils.getLogMsgId(), summary);
        result_page_summary.setText(summary);
        result_page_summary.setVisibility(View.VISIBLE);
        result_summarydivider.setVisibility(View.VISIBLE);

        searchResultsList = new ArrayList<String>();

        for (index = 0; index < total_results; index++) {
            book = searchResults.getBookResult(index);
            Log.d("#### "+Utils.getLogMsgId(), "Title : " + book.getTitle());
            Log.d("#### "+Utils.getLogMsgId(), "Author : " + book.getName());
            Log.d("#### "+Utils.getLogMsgId(), "Image : " + book.getImage_url());
            searchResultsList.add(book.getTitle()+" By "+book.getName());
        }

        searchResultsDisplay.setAdapter(new myAdaptor());
    }

    private class myAdaptor extends BaseAdapter {
        public myAdaptor() {

        }

        @Override
        public int getCount() {
            return searchResultsList.size();
        }

        @Override
        public Object getItem(int position) {
            return searchResultsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        public class viewHolder {
            TextView book_name;
            TextView author_name;
            ImageView bookImage;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            viewHolder Holder;
            bookResult book;

            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.book_search_result,null);
                Holder = new viewHolder();
                Holder.book_name = (TextView)convertView.findViewById(R.id.book_name);
                Holder.author_name = (TextView)convertView.findViewById(R.id.author_name);
                Holder.bookImage = (ImageView)convertView.findViewById(R.id.bookimage);
                convertView.setTag(Holder);
            } else {
                Holder = (viewHolder) convertView.getTag();
            }

            Button add_to_bookshelf = (Button) convertView.findViewById(R.id.add_to_my_bookshelf_button);

            book = results.getBookResult(position);
            add_to_bookshelf.setTag(book);
            add_to_bookshelf.setOnClickListener(add_to_bookshelf_on_click_listner);
            Holder.book_name.setText(book.getTitle());
            Holder.author_name.setText("By " + book.getName());
            Log.d("#### "+Utils.getLogMsgId(), "LOADING IMAGE...");
            if (book.getImage_url() != null) {
                Picasso.with(getActivity()).load(book.getImage_url())
                        .into(Holder.bookImage);
            } else {
                Holder.bookImage.setImageResource(R.drawable.no_book_cover);
            }
            return convertView;
        }

        private View.OnClickListener add_to_bookshelf_on_click_listner = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toBeAdded = (bookResult) v.getTag();
                Log.d("#### "+Utils.getLogMsgId(), "Clicked add to bookshelf to add " + toBeAdded.getTitle() + " by " + toBeAdded.getName());

                final Dialog price_dialog = new Dialog((WelcomeActivity) getActivity());
                price_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                price_dialog.setContentView(R.layout.book_prices_set);
                //price_dialog.setTitle("Set rent and resale price...");
                searchProgressBar.setVisibility(View.INVISIBLE);
                final EditText rent_pr_et = (EditText)price_dialog.findViewById(R.id.rent_price_et);
                final EditText resale_pr_et = (EditText)price_dialog.findViewById(R.id.resale_price_et);
                Button ok_b = (Button) price_dialog.findViewById(R.id.ok_button);
                //Button cancel_b = (Button) price_dialog.findViewById(R.id.cancel_button);
                ok_b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String rent_prs = rent_pr_et.getText().toString();
                        String resale_prs = resale_pr_et.getText().toString();
                        if (rent_prs.matches("") || resale_prs.matches("")) {
                            Toast.makeText((WelcomeActivity) getActivity(), "Please fill both the fields.", Toast.LENGTH_LONG).show();
                            return;
                        }
                        Log.d ("#### "+Utils.getLogMsgId(), "Rent Price: "+ rent_prs+" Resale Price: "+resale_prs);

                        toBeAdded.setRent_price(rent_prs);
                        toBeAdded.setResale_price(resale_prs);
                        String fetch_isbn_query_url=GET_ISBN_API+toBeAdded.getBook_id()+"?format=xml&key="+key;
                        Log.d("#### "+Utils.getLogMsgId(), "Get ISBN with this query " + fetch_isbn_query_url);
                        getContent(fetch_isbn_query_url, ACTION_FOR_INTENT_CALLBACK);
                        searchProgressBar.setVisibility(View.VISIBLE);
                        price_dialog.cancel();
                    }
                });
                /*
                cancel_b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        price_dialog.cancel();
                    }
                });
                */
                price_dialog.show();
            }
        };
    }

    public void add_isbn_to_the_user(String isbn13) {
        if (isbn13 == null) {
            ((WelcomeActivity)getActivity()).showDialogMsg("Error", "Adding e-books to bookshelf is not supported");
        } else {
            Log.d("#### "+Utils.getLogMsgId(), "ISBN13 = " + isbn13);
            QUERY_URL = GET_BOOK_DETAILS_API+"ISBN:"+isbn13;
            isbn13_of_book = isbn13;
            getContent(QUERY_URL, ACTION_FOR_GOOGLEAPI_URL);
        }
    }

    public void parse_isbn_response (String response) {
        XmlPullParserFactory xmlFactoryObject = null;
        try {
            xmlFactoryObject = XmlPullParserFactory.newInstance();
            XmlPullParser myParser = xmlFactoryObject.newPullParser();
            myParser.setInput(new StringReader(response));

            String currentTag = null;
            String closingTag = null;

            String isbn13 = null;
            boolean found_isbn13 = false;
            int event = myParser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT && found_isbn13 == false)
            {
                switch (event){
                    case XmlPullParser.START_TAG:
                        currentTag = myParser.getName();
                        break;

                    case XmlPullParser.TEXT:
                        if (currentTag == null)
                            break;

                        if (currentTag.equals("isbn13")) {
                            isbn13= myParser.getText();
                            found_isbn13 = true;
                            break;
                        }

                    case XmlPullParser.END_TAG:
                        closingTag = myParser.getName();
                        if ((currentTag != null) && ( currentTag.equals(closingTag))) {
                            currentTag = null;
                            closingTag = null;
                        }
                        break;
                }
                event = myParser.next();
            }

            Log.d("#### "+Utils.getLogMsgId(), "About to add " + isbn13);
            toBeAdded.setIsbn13(isbn13);
            ((WelcomeActivity) getActivity()).saveBookIsbn(isbn13, toBeAdded.getName(), toBeAdded.getTitle(),
                    toBeAdded.getImage_url(),toBeAdded.getRent_price(),
                                                           toBeAdded.getResale_price());
            //BYPASSING GOOGLE CALL HERE FOR NOW
            //add_isbn_to_the_user(isbn13);

        } catch (XmlPullParserException e) {
            e.printStackTrace();
            return;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void parse_response (String response) {
        XmlPullParserFactory xmlFactoryObject = null;
        try {
            xmlFactoryObject = XmlPullParserFactory.newInstance();
            XmlPullParser myParser = xmlFactoryObject.newPullParser();
            myParser.setInput(new StringReader(response));

            String currentTag = null;
            String closingTag = null;

            Log.d ("#### "+Utils.getLogMsgId(), response);

            bookSearchResults searchResults = new bookSearchResults();

            String book_id = null;
            int event = myParser.getEventType();
            boolean fetch_isbn_parse = false;
            while (event != XmlPullParser.END_DOCUMENT && fetch_isbn_parse == false)
            {
                switch (event){
                    case XmlPullParser.START_TAG:
                        currentTag = myParser.getName();
                        break;

                    case XmlPullParser.TEXT:
                        if (currentTag == null)
                            break;

                        if (currentTag.equals("method")) {
                            if (myParser.getText().equals("book_show")) {
                                Log.d("#### "+Utils.getLogMsgId(), "It's a fetch ISBN response");
                                fetch_isbn_parse = true;
                                break;
                            }
                        }

                        if (currentTag.equals("query-time-seconds"))
                            Log.d ("#### "+Utils.getLogMsgId(), "Total Time Taken : "+myParser.getText());

                        if (currentTag.equals("results-start")) {
                            String data = myParser.getText();
                            searchResults.setResult_start(Integer.parseInt(data));
                        }

                        if (currentTag.equals("results-end")) {
                            String data = myParser.getText();
                            searchResults.setResult_end(Integer.parseInt(data));
                        }

                        if (currentTag.equals("total-results")) {
                            //Log.d("#### "+Utils.getLogMsgId(), "Total Results : " + myParser.getText());
                            String data = myParser.getText();
                            searchResults.setTotal_results(Integer.parseInt(data));
                        }

                        if (currentTag.equals("id")) {
                            //Log.d("#### "+Utils.getLogMsgId(), "Title : " + myParser.getText());
                            book_id = myParser.getText();
                        }

                        if (currentTag.equals("title")) {
                            //Log.d("#### "+Utils.getLogMsgId(), "Title : " + myParser.getText());
                            searchResults.setBookResultTitle(myParser.getText());
                            Log.d("#### "+Utils.getLogMsgId(), "book id = " + book_id);
                            searchResults.setBook_id(book_id);
                        }

                        if (currentTag.equals("name")) {
                            //Log.d("#### "+Utils.getLogMsgId(), "Name : " + myParser.getText());
                            searchResults.setBookResultName(myParser.getText());
                        }

                        if (currentTag.equals("image_url")) {
                            //Log.d("#### "+Utils.getLogMsgId(), "Image URL : " + myParser.getText());
                            if (myParser.getText().contains("/nophoto/")) {
                                searchResults.setBookResultImage_Url(null);
                            } else
                                searchResults.setBookResultImage_Url(myParser.getText());
                        }
                        break;

                    case XmlPullParser.END_TAG:
                        closingTag = myParser.getName();
                        if ((currentTag != null) && ( currentTag.equals(closingTag))) {
                            currentTag = null;
                            closingTag = null;
                        }
                        break;
                }
                event = myParser.next();
            }

            if (fetch_isbn_parse != true)
                display_results (searchResults);
            else
                parse_isbn_response(response);

        } catch (XmlPullParserException e) {
            e.printStackTrace();
            return;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
