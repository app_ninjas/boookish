package com.itz_mine.mark_i;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Intent;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * A fragment that launches other parts of the demo application.
 */
public class SearchFragment extends android.support.v4.app.Fragment {

    MapView mMapView;
    private GoogleMap googleMap;
    public SeekBar seekbar;
    private double latitude, latitude1, latitude2, latitude3, latitude4, latitude5, latitude6;
    private double longitude, longitude1, longitude2, longitude3, longitude4, longitude5, longitude6;
    public int circle_size = 0;
    public double distance;
    public TextView setcircle;
    private int seekBarProgressValue;
    SearchView search;
    viewUserFragment view_User_Fragment;
    ArrayList<user_creds> nearby_users;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflate and return the layout
        View v = inflater.inflate(R.layout.fragment_search, container,
                false);
        search = (SearchView) v.findViewById(R.id.searchView);
        search.setQueryHint("Search Books");
        setcircle = (TextView) v.findViewById(R.id.SetCircle);
        seekbar = (SeekBar) v.findViewById(R.id.seekBar);

        seekBarProgressValue = 0;
        nearby_users = null;
        view_User_Fragment = new viewUserFragment();

        //*** setOnQueryTextFocusChangeListener ***


        search.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub

                //Toast.makeText(getActivity(), String.valueOf(hasFocus),Toast.LENGTH_SHORT).show();
            }
        });

        //*** setOnQueryTextListener ***
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                // TODO Auto-generated method stub
                Log.d("#### "+Utils.getLogMsgId(), "INSIDE onQueryTextSubmit");
                String search_string = search.getQuery().toString();
                fetchUsersWithBooks(search_string);
                return false;
            }


            @Override
            public boolean onQueryTextChange(String newText) {
                // TODO Auto-generated method stub

                //Toast.makeText(getActivity(), newText,Toast.LENGTH_SHORT).show();
                return false;
            }
        });


        mMapView = (MapView) v.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                _onMapReady(googleMap);
            }
        });

        // Perform any camera updates here

        Log.d("#### "+Utils.getLogMsgId(), String.valueOf(distance));
        return v;
    }

    public void _onMapReady(final GoogleMap map) {
        googleMap = map;
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("#### "+Utils.getLogMsgId(), "no permission");
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    5);
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.


        }
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        final MarkerOptions markerOptions = new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        GPSTracker gps = new GPSTracker(getActivity());
        gps.getLocation();
        latitude = gps.latitude;
        longitude = gps.longitude;
        fetchNearbyUsers(latitude, longitude, 3);

        markerOptions.position(new LatLng(latitude, longitude));
        // markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.logo));
        markerOptions.title("Current Location");

        markerOptions.draggable(true);
        googleMap.addMarker(markerOptions);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 13));

        googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker markerDragStart) {
                // TODO Auto-generated method stub
                Log.e("#### "+Utils.getLogMsgId(), "Marker Start ");
            }

            @Override
            public void onMarkerDragEnd(Marker markerDragEnd) {
                Log.e("#### "+Utils.getLogMsgId(), "Marker End ");
                latitude = markerDragEnd.getPosition().latitude;
                longitude = markerDragEnd.getPosition().longitude;
                Log.e("#### "+Utils.getLogMsgId(), String.valueOf(latitude));
                Log.e("#### "+Utils.getLogMsgId(), String.valueOf(latitude));

            }

            @Override
            public void onMarkerDrag(Marker markerDrag) {

                Log.e("#### "+Utils.getLogMsgId(), "Marker ");
            }
        });

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                MarkerOptions marker = new MarkerOptions().position(
                        new LatLng(latLng.latitude, latLng.longitude)).title("New Marker").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                latitude = latLng.latitude;
                longitude = latLng.longitude;
                googleMap.clear();
                googleMap.addMarker(marker);
                fetchNearbyUsers(latitude, longitude, seekBarProgressValue);
            }
        });

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Bundle userdata = new Bundle();
                if (nearby_users != null) {
                    for (int i = 0; i < nearby_users.size(); i++) {
                        double users_latitude = nearby_users.get(i).getPoint().getLatitude();
                        double users_longitude = nearby_users.get(i).getPoint().getLongitude();
                        if (nearby_users.get(i).getFull_name().equals(marker.getTitle())) {
                            Log.d("#### "+Utils.getLogMsgId(), "User Match in Marker. Looking for email id match");
                            if (marker.getSnippet().equals(nearby_users.get(i).getUser_id())) {
                                userdata.putSerializable("user", nearby_users.get(i));
                                ((WelcomeActivity) getActivity()).goToViewUser(userdata);

                                //Toast.makeText(getActivity(),marker.getTitle(),Toast.LENGTH_LONG).show();
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                    }
                }
                return false;
            }
        });

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override

            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {

                googleMap.clear();
                circle_size = (progresValue) * 10 * 10 * 10;
                Log.d("#### "+Utils.getLogMsgId(), "ProgressValue = " + progresValue);
                int radiusinkms = circle_size / 1000;
                seekBarProgressValue = radiusinkms;
                Log.d("#### "+Utils.getLogMsgId(), "SeekBarProgVal = " + seekBarProgressValue);
                //Log.d("#### "+Utils.getLogMsgId(), String.valueOf(circle_size));
                //Log.e("#### "+Utils.getLogMsgId(), String.valueOf(latitude));
                Circle circle = googleMap.addCircle(new CircleOptions()
                        .center(new LatLng(latitude, longitude))
                        .radius(circle_size)
                        .strokeWidth(3)
                        .strokeColor(Color.RED)
                        .fillColor(0x60cdb7b5));
                MarkerOptions markerOptions = new MarkerOptions();
                LatLng latLng = new LatLng(latitude, longitude);
                markerOptions.position(latLng);
                markerOptions.title("Current Location");
                markerOptions.draggable(true);
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                googleMap.addMarker(markerOptions);
                ParseGeoPoint current_location = new ParseGeoPoint(latitude, longitude);

                if (nearby_users != null) {
                    for (int i = 0; i < nearby_users.size(); i++) {
                        if (nearby_users.get(i).getPoint() == null)
                            continue;
                        double users_latitude = nearby_users.get(i).getPoint().getLatitude();
                        double users_longitude = nearby_users.get(i).getPoint().getLongitude();
                        double dist = distFrom(latitude, longitude, users_latitude, users_longitude);
                        Log.d("#### "+Utils.getLogMsgId(), "Distance = " + dist + " Radius = " + radiusinkms);
                        if (dist <= radiusinkms) {
                            markerOptions.position(new LatLng(users_latitude, users_longitude));
                            markerOptions.title(nearby_users.get(i).getFull_name());
                            markerOptions.snippet(nearby_users.get(i).getUser_id());
                            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
                            googleMap.addMarker(markerOptions);
                        }
                    }
                }

                setcircle.clearComposingText();
                setcircle.setText("Set to ");
                setcircle.append(String.valueOf(radiusinkms));
                if (radiusinkms >= 1)
                    setcircle.append(" kms");
                else
                    setcircle.append(" km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });
    }


    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 5: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("#### "+Utils.getLogMsgId(), "permission granted");

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    public void fetchUsersWithBooks (String search_string) {
        /* Fetch user info from user_creds db */

        List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();

        nearby_users = new ArrayList<user_creds>();
        ParseQuery query = new ParseQuery("books");
        query.whereEqualTo("title", search_string);
        queries.add(query);

        query = new ParseQuery("books");
        query.whereEqualTo("author", search_string);
        queries.add(query);

        ParseQuery<ParseObject> mainQuery = ParseQuery.or(queries);

        mainQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    if (list.size() == 0) {
                        int oldProgressValue = 0;
                        oldProgressValue = seekBarProgressValue;
                        seekbar.setProgress(0);
                        seekbar.setProgress(oldProgressValue);
                    }
                    for (int j = 0; j < list.size(); j++) {
                        ArrayList<String> bookResultUsers = (ArrayList<String>) list.get(j).get("users");
                        List<ParseQuery<ParseObject>> user_queries = new ArrayList<ParseQuery<ParseObject>>();
                        for (int i = 0; i < bookResultUsers.size(); i++) {
                            ParseQuery user_query = new ParseQuery("user_creds");
                            user_query.whereEqualTo("user_id", (String) bookResultUsers.get(i));
                            user_queries.add(user_query);
                        }
                        Log.d("#### "+Utils.getLogMsgId(), "Book Matches Found : "+(String) list.get(j).get("title"));
                        ParseQuery<ParseObject> mainUserQuery = ParseQuery.or(user_queries);
                        mainUserQuery.findInBackground(new FindCallback<ParseObject>() {
                            @Override
                            public void done(List<ParseObject> list, ParseException e) {
                                if (e == null) {
                                    if (list.size() == 0) {
                                        int oldProgressValue = 0;
                                        oldProgressValue = seekBarProgressValue;
                                        seekbar.setProgress(0);
                                        seekbar.setProgress(oldProgressValue);
                                    }
                                    for (int j = 0; j < list.size(); j++) {
                                        boolean is_already_present_in_search_results = false;
                                        for (int p = 0; p < nearby_users.size(); p++) {
                                            if (nearby_users.get(p).getUser_id().equals(list.get(j).get("user_id"))) {
                                                is_already_present_in_search_results = true;
                                            }
                                        }
                                        if (is_already_present_in_search_results == false) {
                                            user_creds nu = new user_creds();
                                            nu.setUser_id((String) list.get(j).get("user_id"));
                                            nu.setFull_name((String) list.get(j).get("full_name"));
                                            nu.setPoint((ParseGeoPoint) list.get(j).get("location"));
                                            nearby_users.add(nu);
                                            Log.d("#### "+Utils.getLogMsgId(), "User with Book Matches : " + (String) list.get(j).get("full_name"));
                                        }
                                    }
                                    int oldProgressValue = 0;
                                    oldProgressValue = seekBarProgressValue;
                                    seekbar.setProgress(0);
                                    seekbar.setProgress(oldProgressValue);
                                } else {
                                    Log.d("#### "+Utils.getLogMsgId(), e.getMessage());
                                }
                            }
                        });

                    }
                } else {
                    Log.d("#### "+Utils.getLogMsgId(), e.getMessage());
                }
            }
        });
    }

    public void fetchNearbyUsers (double latitude, double longitude, final int progressVal) {
        /* Fetch user info from user_creds db */
        ParseQuery query = new ParseQuery("user_creds");
        query.whereWithinKilometers("location",
                new ParseGeoPoint(latitude, longitude),
                10);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    nearby_users = new ArrayList<user_creds>();
                    for (int j = 0; j < list.size(); j++) {
                        user_creds nu = new user_creds();
                        nu.setUser_id((String) list.get(j).get("user_id"));
                        nu.setFull_name((String) list.get(j).get("full_name"));
                        nu.setPoint((ParseGeoPoint) list.get(j).get("location"));
                        nu.setDOB((Date) list.get(j).get("DOB"));
                        nu.setGender((String) list.get(j).get("gender"));
                        nu.setMobile_number((String) list.get(j).get("mobile_number"));
                        nu.setBook_isbns((ArrayList<String>) list.get(j).get("book_isbns"));
                        nu.setAddress((String) list.get(j).get("address"));
                        nu.setProfilePhotoFile((ParseFile) list.get(j).get("profile_photo"));
                        nearby_users.add(nu);
                        Log.d("#### "+Utils.getLogMsgId(), "Nearby User : "+(String) list.get(j).get("user_id"));
                    }
                    seekbar.setProgress(progressVal);
                } else {
                    Log.d("#### "+Utils.getLogMsgId(), e.getMessage());
                }
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    public static double distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371.0; // miles (or 6371.0 kilometers)
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double dist = earthRadius * c;

        return dist;
    }

    /*
    protected void search(List<Address> addresses) {

        Address address = (Address) addresses.get(0);
        double home_long = address.getLongitude();
        double home_lat = address.getLatitude();
        LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());

        String addressText = String.format(
                "%s, %s",
                address.getMaxAddressLineIndex() > 0 ? address
                        .getAddressLine(0) : "", address.getCountryName());

        MarkerOptions markerOptions = new MarkerOptions();

        markerOptions.position(latLng);
        markerOptions.title(addressText);

        googleMap.clear();
        googleMap.addMarker(markerOptions);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));


    }
    */
}