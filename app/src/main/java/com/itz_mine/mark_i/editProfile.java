package com.itz_mine.mark_i;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.app.Fragment;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.ParseGeoPoint;
import com.pkmmte.view.CircularImageView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class editProfile extends Fragment {

    user_creds user;
    public String mobilenumber;
    public int mobverify = 0;
    public EditText mobilenum;
    View fragmentView;
    TextView full_name_button;
    TextView agetext;
    TextView gendertext;
    TextView mobnum;
    TextView addrview;
    ImageView blurredImageView;
    ProgressBar imageUploadProgressBar;

    public editProfile() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        fragmentView =  inflater.inflate(R.layout.fragment_edit_profile, container, false);
        full_name_button = (TextView) fragmentView.findViewById(R.id.full_name_button);
        agetext = (TextView) fragmentView.findViewById(R.id.dob_button);
        gendertext = (TextView) fragmentView.findViewById(R.id.gender_button);
        mobnum = (TextView) fragmentView.findViewById(R.id.add_phone_num);
        addrview = (TextView) fragmentView.findViewById(R.id.location_button);
        mobilenum = (EditText) fragmentView.findViewById(R.id.editText1);
        imageUploadProgressBar = (ProgressBar) fragmentView.findViewById(R.id.imgUploadProgressBar);

        blurredImageView = (ImageView) fragmentView.findViewById(R.id.blurred_bg);

        Bitmap bg_image;

        setImageUploadProgressBar(false);

        Bundle userdata = this.getArguments();
        updateVals(userdata);

        if (user!=null)
            setProfImages(user.getProfile_pic());

        Log.e("#### "+Utils.getLogMsgId(), "Before Return");
        return fragmentView;
    }

    public void setImageUploadProgressBar (boolean value) {
        if (value == true)
            imageUploadProgressBar.setVisibility(View.VISIBLE);
        else
            imageUploadProgressBar.setVisibility(View.INVISIBLE);
    }

    public void updateVals (Bundle userdata) {
        Date dob;
        int age;
        String gender;
        String phone_number;
        ParseGeoPoint point;
        String address;

        if (userdata == null) {
            Log.e("#### "+Utils.getLogMsgId(), "EMPTY BUNDLE RECEIVED");
        } else
            Log.d("#### "+Utils.getLogMsgId(), "BUNDLE IS NOT EMPTY");

        user = (user_creds) userdata.getSerializable("user");
        if (user == null) {
            Log.e("#### "+Utils.getLogMsgId(), "GOT NULL user_creds FROM BUNDLE");
        } else
            Log.d("#### "+Utils.getLogMsgId(), "Got data from bundle: " + user.getFull_name());

        phone_number = user.getMobile_number();
        if (phone_number != null)
            mobnum.setText(phone_number);
        else
            mobnum.setText("Update Mobile Number");

        address = user.getAddress();
        if (address != null)
            addrview.setText(address);
        else
            addrview.setText("Update Address");

        full_name_button.setText("Full Name  :  " + user.getFull_name());

        gender = user.getGender();
        if (gender == null) {
            gendertext.setText("Update gender");
        }else
            gendertext.setText(gender);

        dob = user.getDOB();
        if (dob != null) {
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            String date = format.format(dob);
            agetext.setText("D.O.B : " + date);
        }
        else
            agetext.setText("Update DOB");

    }

    public void setProfImages (byte[] profile_pic) {
        Bitmap blurredBitmap = createBlurredImage(profile_pic);
        Bitmap originalBitmap;

        Drawable blurredbg = new BitmapDrawable(getResources(), blurredBitmap);

        blurredImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        blurredImageView.getBackground().setColorFilter(Color.BLACK, PorterDuff.Mode.DARKEN);
        blurredImageView.setAlpha(150);
        blurredImageView.setImageDrawable(blurredbg);
    }

    private Bitmap createBlurredImage (byte[] profile_pic) {
        // Load a clean bitmap and work from that.
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;
        Bitmap originalBitmap;
        if (profile_pic == null) {
            Log.e("#### "+Utils.getLogMsgId(), "profile pic is null");
            originalBitmap = BitmapFactory.decodeResource (getResources(), R.drawable.profilepiclarge, options);
        }
        else {
            originalBitmap = BitmapFactory.decodeByteArray(profile_pic, 0, profile_pic.length, options);
        }

        // Create another bitmap that will hold the results of the filter.
        Bitmap blurredBitmap;
        blurredBitmap = Bitmap.createBitmap (originalBitmap);

        // Create the Renderscript instance that will do the work.
        RenderScript rs = RenderScript.create(getActivity());

        // Allocate memory for Renderscript to work with
        Allocation input = Allocation.createFromBitmap (rs, originalBitmap, Allocation.MipmapControl.MIPMAP_FULL, Allocation.USAGE_SCRIPT);
        Allocation output = Allocation.createTyped(rs, input.getType());

        // Load up an instance of the specific script that we want to use.
        ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create (rs, Element.U8_4(rs));
        script.setInput(input);

        // Set the blur radius
        script.setRadius (25);

        // Start the ScriptIntrinisicBlur
        script.forEach(output);

        // Copy the output to the blurred bitmap
        output.copyTo (blurredBitmap);

        return blurredBitmap;
    }




}
