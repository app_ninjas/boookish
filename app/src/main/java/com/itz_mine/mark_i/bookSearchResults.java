package com.itz_mine.mark_i;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by root on 1/6/16.
 */


public class bookSearchResults {
    private int total_results;
    private int result_start;
    private int result_end;
    ArrayList<bookResult> myarray = new ArrayList<bookResult>();
    bookResult book;
    int count;

    public void setTotal_results(int total_results) {
        this.total_results = total_results;
        Log.d("####", "Creating an array of "+total_results+" books");
        count = 0;
    }

    public int getResult_start() {
        return result_start;
    }

    public void setResult_start(int result_start) {
        this.result_start = result_start;
    }

    public int getResult_end() {
        return result_end;
    }

    public void setResult_end(int result_end) {
        this.result_end = result_end;
    }

    public void setBookResultTitle(String title) {
        book = new bookResult();
        book.setTitle(title);
    }

    public void setBookResultName(String name) {
        book.setName(name);
    }

    public void setBookResultImage_Url(String image_url) {
        book.setImage_url(image_url);
        myarray.add(book);
        count++;
        Log.d ("#### "+Utils.getLogMsgId(), "Added book #"+count);
    }

    public bookResult getBookResult (int index) {
        return myarray.get(index);
    }

    public int getTotal_results() {
        return total_results;
    }

    public void setBook_id(String book_id) {
        book.setBook_id(book_id);
    }
}
