package com.itz_mine.mark_i;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.app.Fragment;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.pkmmte.view.CircularImageView;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.SendBirdException;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;


/**
 * A simple {@link Fragment} subclass.
 */
public class viewUserFragment extends Fragment {

    user_creds user;
    ImageView blurredImageView;
    CircularImageView circularImageView;
    ArrayList<books> users_books;
    ListView book_display;
    TextView msg_txtview;
    Bundle userdata;

    public viewUserFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        Date dob;
        int age;
        users_books = new ArrayList<books>();
        View fragmentView =  inflater.inflate(R.layout.fragment_view_user, container, false);
        TextView txtuser = (TextView) fragmentView.findViewById(R.id.welcome_textview);
        TextView phn_num = (TextView) fragmentView.findViewById(R.id.phn_num);
        book_display = (ListView) fragmentView.findViewById(R.id.book_display);
        book_display.setVisibility(View.INVISIBLE);

        blurredImageView = (ImageView) fragmentView.findViewById(R.id.blurred_bg);
        circularImageView = (CircularImageView)fragmentView.findViewById(R.id.circularImageView);
        msg_txtview = (TextView)fragmentView.findViewById(R.id.Message);

        Bitmap bg_image;
        //ProgressBar imageUploadProgressBar = (ProgressBar) fragmentView.findViewById(R.id.imgUploadProgressBar);
        //imageUploadProgressBar.setVisibility(View.INVISIBLE);
        userdata = null;
        userdata = this.getArguments();
        if (userdata == null) {
            Log.e("#### "+ Utils.getLogMsgId(), "EMPTY BUNDLE RECEIVED");
        } else
            Log.d("#### "+ Utils.getLogMsgId(), "BUNDLE IS NOT EMPTY");

        user = (user_creds) userdata.getSerializable("user");
        if (user == null) {
            Log.e ("#### "+ Utils.getLogMsgId(), "GOT NULL user_creds FROM BUNDLE");
        } else
            Log.d("#### "+ Utils.getLogMsgId(), "Got data from bundle: " + user.getFull_name());

        txtuser.setText(user.getFull_name());
        String phone_number = user.getMobile_number();
        if (phone_number != null) {
            //phn_num.setText(phone_number);
        }

        getISBNData();
        getDisplayPic();

        msg_txtview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> userids = new ArrayList<String>();
                userids.add(user.getUser_id());
                ParseUser currentUser = ParseUser.getCurrentUser();
                userids.add(currentUser.get("user_id").toString());
                Log.d("#### "+ Utils.getLogMsgId(), "Current User: " + currentUser.get("user_id").toString());
                GroupChannel.createChannelWithUserIds(userids, true, new GroupChannel.GroupChannelCreateHandler() {
                    @Override
                    public void onResult(GroupChannel groupChannel, SendBirdException e) {
                        if (e == null) {
                            Log.d("#### "+ Utils.getLogMsgId(), "Created channel for chat");
                            user.setChannel_url(groupChannel.getUrl());
                            userdata.putSerializable("user", user);
                            ((WelcomeActivity) getActivity()).chatwithuser(userdata);
                        } else
                            Log.e("#### "+ Utils.getLogMsgId(), "Failed to create channel for chat. Error: " + e.getMessage());
                    }
                });
            }
        });

        return fragmentView;
    }

    public void getDisplayPic () {
        ParseFile file = user.getProfilePhotoFile();
        if (file != null) {
            file.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] bytes, ParseException e) {
                    if (e == null) {
                        user.setProfile_pic(bytes);
                        // data has the bytes for the image
                    } else {
                        Log.e("#### "+ Utils.getLogMsgId(), e.getMessage());
                    }
                    setProfImages(user.getProfile_pic());
                }
            });
        } else {
            Log.d ("#### "+ Utils.getLogMsgId(), "No display pic is set for the user.");
            setProfImages(user.getProfile_pic());
        }
    }

    public void updateUserBooks () {
        if (user.getUserbooks() != null) {
            int num_of_books = users_books.size();
            Log.d("#### "+ Utils.getLogMsgId(), "num_of_books = "+num_of_books);
            book_display.setVisibility(View.VISIBLE);
            book_display.setAdapter(new myBookDisplayAdaptor());
        } else {
            Log.d("#### "+ Utils.getLogMsgId(), "No books found for user");
        }
    }

    private class myBookDisplayAdaptor extends BaseAdapter {
        public myBookDisplayAdaptor() {

        }

        @Override
        public int getCount() {
            return users_books.size();
        }

        @Override
        public Object getItem(int position) {
            return users_books.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        public class viewHolder {
            TextView book_name;
            TextView author_name;
            ImageView bookImage;
            Button rent_Button;
            Button buy_Button;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            viewHolder Holder;
            books book;

            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.books_display,null);
                Holder = new viewHolder();
                Holder.book_name = (TextView)convertView.findViewById(R.id.book_name);
                Holder.author_name = (TextView)convertView.findViewById(R.id.author_name);
                Holder.bookImage = (ImageView)convertView.findViewById(R.id.bookimage);
                Holder.rent_Button = (Button)convertView.findViewById(R.id.rent_button);
                Holder.buy_Button = (Button)convertView.findViewById(R.id.buy_button);
                convertView.setTag(Holder);
            } else {
                Holder = (viewHolder) convertView.getTag();
            }

            Button rent = (Button) convertView.findViewById(R.id.rent_button);
            Button buy = (Button) convertView.findViewById(R.id.buy_button);

            book = users_books.get(position);
            rent.setTag(book);
            rent.setOnClickListener(rent_on_click_listner);
            buy.setTag(book);
            buy.setOnClickListener(buy_on_click_listner);
            Holder.book_name.setText(book.getBook_name());
            Holder.author_name.setText("By " + book.getAuthor_name());
            Holder.rent_Button.setText("Rent : Rs."+book.getRent_price()+" \\-");
            Holder.buy_Button.setText("Buy : Rs."+book.getResale_price()+" \\-");
            Log.d("#### "+ Utils.getLogMsgId(), "LOADING IMAGE...");
            if (book.getImageLinks() != null) {
                Picasso.with(getActivity()).load(book.getImageLinks())
                        .into(Holder.bookImage);
            } else {
                Holder.bookImage.setImageResource(R.drawable.no_book_cover);
            }
            return convertView;
        }

        private void send_request_msg_to_other_user (final String msg) {
            ArrayList<String> userids = new ArrayList<String>();
            userids.add(user.getUser_id());
            ParseUser currentUser = ParseUser.getCurrentUser();
            userids.add(currentUser.get("user_id").toString());
            Log.d("#### "+ Utils.getLogMsgId(), "Current User: " + currentUser.get("user_id").toString());
            GroupChannel.createChannelWithUserIds(userids, true, new GroupChannel.GroupChannelCreateHandler() {
                @Override
                public void onResult(GroupChannel groupChannel, SendBirdException e) {
                    if (e == null) {
                        Log.d("#### "+ Utils.getLogMsgId(), "Created channel for chat");
                        user.setChannel_url(groupChannel.getUrl());
                        user.setMsg(msg);
                        userdata.putSerializable("user", user);
                        ((WelcomeActivity) getActivity()).chatwithuser(userdata);
                    } else
                        Log.e("#### "+ Utils.getLogMsgId(), "Failed to create channel for chat. Error: " + e.getMessage());
                }
            });
        }

        private View.OnClickListener rent_on_click_listner = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                books book = (books) v.getTag();
                Log.d ("#### "+ Utils.getLogMsgId(), "Clicked on Rent "+book.getBook_name()+" for Rs."+book.getRent_price()+" \\-");

                final String msg = "Hi! I would like to rent "+book.getBook_name();
                send_request_msg_to_other_user(msg);
            }
        };

        private View.OnClickListener buy_on_click_listner = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                books book = (books) v.getTag();
                Log.d ("#### "+ Utils.getLogMsgId(), "Clicked on Buy "+book.getBook_name()+" for Rs."+book.getResale_price()+" \\-");

                final String msg = "Hi! I would like to buy "+book.getBook_name();
                send_request_msg_to_other_user(msg);
            }
        };
    }

    public void getISBNData () {
        Log.d("#### "+ Utils.getLogMsgId(), "Inside getISBNData");

        List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();

        if (user.getBook_isbns() == null) {
            return;
        }

        final HashMap hm = new HashMap();

        for (int i =0; i<user.getBook_isbns().size(); i++) {
            StringTokenizer st = new StringTokenizer(user.getBook_isbns().get(i));
            String isbn_num = st.nextToken("-");
            String rent_price = st.nextToken("-");
            String resale_price = st.nextToken("-");
            ParseQuery<ParseObject> query = ParseQuery.getQuery("books");
            query.whereEqualTo("isbn", isbn_num);
            queries.add(query);
            Log.d("#### "+ Utils.getLogMsgId(), user.getBook_isbns().get(i));
            hm.put(isbn_num, rent_price+"-"+resale_price);
        }

        ParseQuery<ParseObject> mainQuery = ParseQuery.or(queries);

        mainQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    for (int j = 0; j < objects.size(); j++) {
                        books Book = new books();
                        Book.setBook_isbn((String) objects.get(j).get("isbn"));
                        Book.setBook_name((String) objects.get(j).get("title"));
                        Book.setAuthor_name((String) objects.get(j).get("author"));
                        Book.setImageLinks((String) objects.get(j).get("image_url"));
                        Book.setUsers((List<String>) objects.get(j).get("users"));
                        String prices = (String) hm.get(Book.getBook_isbn());
                        StringTokenizer st = new StringTokenizer(prices);
                        Book.setRent_price(st.nextToken("-"));
                        Book.setResale_price(st.nextToken("-"));
                        users_books.add(Book);
                        Log.d("#### "+ Utils.getLogMsgId(), (String) objects.get(j).get("image_url"));
                    }
                    user.setUserbooks((ArrayList<books>) users_books);
                } else {
                    Log.d("#### "+ Utils.getLogMsgId(), "Error in fetching ISBN data: " + e.getMessage());
                }
                updateUserBooks();
            }
        });
    }

    public void setProfImages (byte[] profile_pic) {
        Bitmap blurredBitmap = createBlurredImage(profile_pic);
        Bitmap originalBitmap;

        Drawable blurredbg = new BitmapDrawable(getResources(), blurredBitmap);

        blurredImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        blurredImageView.getBackground().setColorFilter(Color.BLACK, PorterDuff.Mode.DARKEN);
        blurredImageView.setAlpha(150);
        blurredImageView.setImageDrawable(blurredbg);

        if (profile_pic == null) {
            Log.e("#### "+ Utils.getLogMsgId(), "profile pic is null");
            originalBitmap = BitmapFactory.decodeResource (getResources(), R.drawable.profilepiclarge);
        } else {
            originalBitmap = BitmapFactory.decodeByteArray(profile_pic, 0, profile_pic.length);
        }

        Drawable circularDrawable = new BitmapDrawable(getResources(), originalBitmap);
        circularImageView.addShadow();
        circularImageView.setImageDrawable(circularDrawable);
    }

    private Bitmap createBlurredImage (byte[] profile_pic) {
        // Load a clean bitmap and work from that.
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;
        Bitmap originalBitmap;
        if (profile_pic == null) {
            Log.e("#### "+ Utils.getLogMsgId(), "profile pic is null");
            originalBitmap = BitmapFactory.decodeResource (getResources(), R.drawable.profilepiclarge, options);
        }
        else {
            originalBitmap = BitmapFactory.decodeByteArray(profile_pic, 0, profile_pic.length, options);
        }

        // Create another bitmap that will hold the results of the filter.
        Bitmap blurredBitmap;
        blurredBitmap = Bitmap.createBitmap (originalBitmap);

        // Create the Renderscript instance that will do the work.
        RenderScript rs = RenderScript.create(getActivity());

        // Allocate memory for Renderscript to work with
        Allocation input = Allocation.createFromBitmap (rs, originalBitmap, Allocation.MipmapControl.MIPMAP_FULL, Allocation.USAGE_SCRIPT);
        Allocation output = Allocation.createTyped(rs, input.getType());

        // Load up an instance of the specific script that we want to use.
        ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        script.setInput(input);

        // Set the blur radius
        script.setRadius(25);

        // Start the ScriptIntrinisicBlur
        script.forEach(output);

        // Copy the output to the blurred bitmap
        output.copyTo (blurredBitmap);

        return blurredBitmap;
    }

}
